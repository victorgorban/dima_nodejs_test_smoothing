import { config } from 'dotenv'
import _ from 'lodash';
import fs from 'fs';
import { createCanvas } from 'canvas';

import testTasks from './testTasks.js';
import testShot1 from './testShots/k9FcurciNvbv844xq.js';
import testShot2 from './testShots/pZR7grQkmp5ee4iXp.js';
import testShot3 from './testShots/SrCDWdnXfpqiStsru.js';
import testShot4 from './testShots/W3rR2sKLfD6xSsYsa.js';

import testShot5 from './shots_without_classes/77NHPyMYgtSREEwwp.js';
import testShot6 from './shots_without_classes/AQBhXfr7BJMkEGbBy.js';
import testShot7 from './shots_without_classes/CkvKEamRgZMbq6yhW.js';
import testShot8 from './shots_without_classes/MHJpFPvtkMW9FnZ3A.js';
import testShot9 from './shots_without_classes/NnBLPPp3tPWFqwPt6.js';
import testShot10 from './shots_without_classes/shmNCtGgroc9eQa9e.js';
import testShot11 from './shots_without_classes/vhASz3PmZS6ftxXqC.js';
import { serverCanvasWidth, serverCanvasHeight, imageBallRadiusServer } from './smoothing/constants.js';
import { handleTask, prepareTaskFromShot, fillShotFromTask } from './smoothing/smoothing.js'
import { detectShotClasses } from './smoothing/detecting_classes.js'
import './smoothing/extensions.js';

config()

launch();

function drawPathOnCanvas(ctx, color, points, isOffsetPath = false) {
  ctx.strokeStyle = color;
  ctx.beginPath();

  for (let point of points) {
    if (isOffsetPath) {
      point = { x: point.dx, y: point.dy };
    }

    ctx.lineTo(point.x, point.y);
  }

  ctx.stroke();
}

function drawCirclesOnCanvas(ctx, color, points, isOffsetPath = false) {
  ctx.strokeStyle = color;

  for (let point of points) {
    if (isOffsetPath) {
      point = { x: point.dx, y: point.dy };
    }

    ctx.beginPath();
    ctx.ellipse(point.x, point.y, imageBallRadiusServer / 2, imageBallRadiusServer / 2, 0, 0, 2 * Math.PI);
    ctx.stroke();
  }
}

async function launch() {
  try {
    // console.log('testTask id', testTask._id);
    // let numTest1 = 4;
    // let numTest2 = 4.324325;
    // console.log('extension', ['f', 'l'].first());
    // , testShot2, testShot3, testShot4
    let shotsToCheck = [testShot1, testShot2, testShot3, testShot4]; // testShot1, testShot2, testShot3, testShot4, testShot5, testShot6, testShot7, testShot8, testShot9, testShot10, testShot11
    for (let shotIndex = 0; shotIndex < shotsToCheck.length; shotIndex++) {
      let shot = shotsToCheck[shotIndex];
      const canvas = createCanvas(1280, 640)
      const ctx = canvas.getContext('2d')
      drawCirclesOnCanvas(ctx, 'white', shot.pointsCloud.map(pd => pd.point), false)
      let [classes, pointsCloud, classifiedPointsData] = detectShotClasses(shot);
      if(!classifiedPointsData?.[0]) continue;

      drawCirclesOnCanvas(ctx, 'red', classifiedPointsData[0].filter(pd => pd.type != 'virtual').map(pd => pd.point), false)
      drawCirclesOnCanvas(ctx, 'yellow', classifiedPointsData[0].filter(pd => pd.type == 'virtual').map(pd => pd.point), false)
      if (classifiedPointsData[1]) {
        drawCirclesOnCanvas(ctx, 'blue', classifiedPointsData[1].map(pd => pd.point), false)
      }
      if (classifiedPointsData[2]) {
        drawCirclesOnCanvas(ctx, 'green', classifiedPointsData[2].map(pd => pd.point), false)
      }
      if (classifiedPointsData[3]) {
        drawCirclesOnCanvas(ctx, 'green', classifiedPointsData[3].map(pd => pd.point), false)
      }
      if (classifiedPointsData[4]) {
        drawCirclesOnCanvas(ctx, 'brown', classifiedPointsData[4].map(pd => pd.point), false)
      }
      
      // for (let key of Object.keys(classifiedPointsData)) {
      //   let points = classifiedPointsData[key]
      //   drawCirclesOnCanvas(ctx, 'green', points.slice(0, 2).map(pd => pd.point), false)
      // }

      let canvasImageBuffer = canvas.toBuffer();
      // как закончу тестить одну, нужно будет прогнать все существующие таски. А затем все доступные шоты.
      await fs.promises.writeFile(`output/shots/${shotIndex}.png`, canvasImageBuffer);
    }

    console.log('Завершено');

  } catch (e) {
    console.error(e)
  }
}