import _ from 'lodash';
import * as utils from './utils.js';
import * as opheim from './opheim_smoothing.js';
import { serverCanvasWidth, serverCanvasHeight, imageBallRadiusServer } from './constants.js';
import './extensions.js';

/**
 * Превращает shot в task. Не вся информация обрабатывается, выставляется только то, что нужно для алгоритма.
 * @param {*} currentShot 
 * @returns currentTask - сформированная задача
 */
export function prepareTaskFromShot(currentShot) {
    if (!currentShot) return;

    const currentTask = {};
    currentTask._id = currentShot._id;
    currentTask.balls = [];

    if (!currentShot.classes) {
        currentShot.classes = prepareShotClasses(currentShot.pointsCloud);
    }

    // самое главное в shot - это classes. Там указано, с какого фрейма начинается движение шара. Если не указано frame, значит фрейм любой, движения нет.
    for (let shotClass of currentShot.classes) {
        const ball = {};
        ball._id = shotClass.class;
        if (ball._id == 0) {
            ball.status = 'cue';
        }


        // в ball.position заносится объект с frame-2, а само движение начинается с frame-1;
        // { "class": 8, "frame": 20 }
        // в position у меня "y": 303, "x": 698 - а это { "z": 18, "y": 303, "x": 698 },
        // path начинается с z: 19, то есть frame-1
        // 

        if (!shotClass.frame) {
            ball.position = currentShot.pointsCloud.find(pointData => pointData.class == ball._id).point;
        } else {
            let classPoints = currentShot.pointsCloud.filter(pointData => pointData.class == ball._id);

            ball.path = classPoints.filter(pointData => pointData.point.z >= shotClass.frame - 1).map(pointData => pointData.point);
            ball.position = classPoints.filter(pointData => pointData.point.z < ball.path.first().z).last().point;
        }
        currentTask.balls.push(ball);
    }

    return currentTask;
}

export function prepareShotClasses(pointsCloud) {
    const classes = [];
    const groupedPointsData = _.groupBy(pointsCloud, 'class');
    for (let [classNumber, pointsDataGroup] of Object.entries(groupedPointsData)) {
        classNumber = +classNumber;
        if (classNumber < 0) break;
        let maxZWithoutMovement = -1;
        for (let index = 1; index < pointsDataGroup.length; index++) {
            const prevPointData = pointsDataGroup[index - 1];
            const currentPointData = pointsDataGroup[index];
            if (currentPointData.point.x != prevPointData.point.x || currentPointData.point.y != prevPointData.point.y) {
                maxZWithoutMovement = prevPointData.point.z;
                // для первого шота, должно получиться [{ "class": 0, "frame": 11 }, { "class": 8, "frame": 20 }, { "class": 9 }]
                classes.add({ class: classNumber, frame: currentPointData.point.z + 1 });
                break;
            }
        }
        if (maxZWithoutMovement == -1 && classNumber >= 0) {
            classes.add({ class: classNumber });
        }
    }

    return classes;
}

/**
 * Заменяет cloudBalls.$.point значениями из currentTask.balls.
 * @param {*} currentShot 
 * @returns currentShot - 
 */
export function fillShotFromTask(currentShot, currentTask) {
    if (!currentShot) return;
    if (!currentTask) return;

    for (let ball of currentTask.balls)
    // интересуют только шары с path
    {
        if (!ball.smoothedPathWithTimings) {
            continue;
        }

        const ballNumber = ball._id;
        for (let pathPoint of ball.smoothedPathWithTimings) {
            let ballPointsCloud = currentShot.pointsCloud.filter(pointData => pointData && pointData.class == ballNumber);
            ballPointsCloud = _.sortBy(ballPointsCloud, ['frame']);
            // в исходных данных всё еще присутствуют разрывы. class 0, нет z=13. Нет гарантии что таких дырок нет еще где-то.
            // и добавлять точки времени в исходные данные мне нельзя...
            let pathPointZ = pathPoint.z;
            let ballPointsCloudWithZLessOrEqualCurrent = ballPointsCloud.filter(pointData => pointData.point.z <= pathPointZ);
            // теперь берем last. Если z равен, то просто обновляем point. Если не равен, то создаем точку и добавляем.
            let pointDataWithZLast = ballPointsCloudWithZLessOrEqualCurrent.last();

            if (pointDataWithZLast.point.z == pathPointZ) {
                pointDataWithZLast.point = pathPoint;
            } else
            // генерируем точку и добавляем
            // почему-то в это место мы попадаем вообще всегда, после того как попали на первый пробел.
            {
                let generatedPoint = { ...pointDataWithZLast, point: pathPoint, frame: pointDataWithZLast.frame + 1, conf: null, type: 'generate' };
                // if(!generatedPoint.rect) {
                //     console.log('упс') // у всех есть rect.
                // }
                currentShot.pointsCloud.push(generatedPoint);
            }

        }
        // короче. Нужно deflectionSectors вместе с z. У всех шаров. А уже потом сюда идти. Иначе хрень будет.
        // а так как изначально deflectionSectors строятся по пути с дырками, то z нужно заносить уже тогда. Потом уже не определишь.
        // если есть начало движения в этих z, то сравнивать ball.position, а не ball.path[0].
        // то есть нужно взять deflectionPoints (и position) других шаров, затем взять сектор z плюс-минус 3 у текущего шара. 
        // И у этих мини-секторов смотреть расстояние. если меньше 2х диаметров шара, то наверое это оно.

        let ballClassData = currentShot.classes.find(obj => obj.class == ballNumber);
        let otherBallsWithPath = currentTask.balls.filter(ball => ball.path && ball._id != ballNumber);
        if (ballClassData) {
            // {frame: videoFrame, point: {x,y,z}, type: 'ball|tableSide'}
            const allIntersectionPointsWithType = [];
            ballClassData.intersectionPoints = allIntersectionPointsWithType;
            // точки пересечения убираем - мешает. Смотрим только начало движения
            // , ...current.deflectionSectors.map(p => p.intersectionPointWithZ)
            let otherBallsIntersectionPointsWithZAndStartPoints = otherBallsWithPath.reduce((all, current) => [...all, current.path.first()], []);

            for (let otherBallIntersectionPoint of otherBallsIntersectionPointsWithZAndStartPoints) {
                const pointWithMinZDistance = ball.smoothedPathWithTimings.find(p => p.z == otherBallIntersectionPoint.z);

                if (pointWithMinZDistance) {
                    allIntersectionPointsWithType.push({ point: pointWithMinZDistance, type: 'ball' });
                }
            }
            // пересечение может быть с другим шаром (в тч повторное), а также с бортом.
            // но при этом точка преломления и начало движения у них может быть и с бортом, и с другим шаром, поэтому придется смотреть на расстояние до этой точки.
            // если не шар, то это или борт, или просто шар посреди поля покатился назад из-за вращения. 

            // теперь идем по intersectionPoints, и ищем пересечения с бортами
            // предположим, нашли точку которая рядом с бортом (x/y около 0 или около max).
            // тут может быть и удар по шару с ударом в борт, и просто удар по шару без касания борта.
            // если идет пересечение с шаром (line1) (круг не увеличивать, т.к. легкое касание уголком все равно отправит биток в борт),
            // то это не считать пересечением с бортом.
            // если продолжить line1 буквально на один кадр (исходя из предпоследней и последней точки line1), и добавить 2 радиуса шара,
            // и точка линии будет за бортом, то это точно пересечение с бортом. Если нет, то это фигня какая-то. 
            // Может быть просто вращение было сильное. Не считать точкой пересечения с бортом.

            function isPointNearSide(offset, maxAllowedDistance = 0) {
                if (offset.dx < maxAllowedDistance ||
                    offset.dy < maxAllowedDistance ||
                    offset.dx > (serverCanvasWidth - maxAllowedDistance) ||
                    offset.dy > (serverCanvasHeight - maxAllowedDistance)) {
                    return true
                } else
                    return false;
            }
            function isPointOutsideTable(offset) {
                return isPointNearSide(offset, 0)
            }
            for (const deflectionSector of ball.deflectionSectors) {
                const intersectionPoint = deflectionSector.intersectionPointWithZ;
                if (isPointNearSide(deflectionSector.intersectionPoint, imageBallRadiusServer * 4)) {
                    let extraDistance = imageBallRadiusServer * 4;
                    // теперь найти среднюю линию угла, так чтобы её продлить.
                    // думаю, достаточно будет взять line1 и line2 (а там примерно равная длина отрезков), найти точку посередине, ну и продлить отрезок куда надо.
                    // продолжаем среднюю линию угла на указанное расстояние. Конечная точка должна быть За столом. 
                    // line1 или line2 не подходят - может быть очень косой угол по отношению к борту.
                    let middleOffset = utils.generateSegmentOffset(deflectionSector.line1.start, deflectionSector.line2.end, 0.5);
                    // deflectionSector.line2.endIndex должен быть больше чем deflectionSector.line2.startIndex
                    // deflectionSector.line1.endIndex должен быть больше чем deflectionSector.line1.startIndex
                    let originalMiddleLength = utils.calculateOffsetsDistance(middleOffset, deflectionSector.line1.end);
                    let segmentDelta = (originalMiddleLength + extraDistance) / originalMiddleLength;
                    let extraOffset = utils.generateSegmentOffset(middleOffset, deflectionSector.line1.end, segmentDelta);
                    if (isPointOutsideTable(extraOffset)) {
                        allIntersectionPointsWithType.push({ point: intersectionPoint, type: 'side' });
                    }
                }
            }

            let ballPointsCloud = currentShot.pointsCloud.filter(pointData => pointData && pointData.class == ballNumber);
            // теперь проставить frame, который взять из pointsCloud (объекты с нужным class и подходящим point.z). Они там точно есть.
            for (const intersectionPointWithType of allIntersectionPointsWithType) {
                const pointData = ballPointsCloud.find(pointData => pointData.point?.z == intersectionPointWithType.point.z);
                if (pointData) {
                    intersectionPointWithType.videoFrame = pointData.frame;
                }
            }
        }
    }

    currentShot.pointsCloud = _.sortBy(currentShot.pointsCloud, ['frame']);
    console.log('currentShot.classes', currentShot.classes)

    return [currentShot.pointsCloud, currentShot.classes];
}

export function handleTask(currentTask) {
    if (!currentTask) {
        return;
    }

    // if(!currentTask.minTimeZ) // вычисление вещей, которые должны быть внутри Task изначально
    if (true) {
        utils.setTaskMinAndMaxTimeZ(currentTask);
        // вычисляем offset-позиции. Фактически переводим точки из одного типа в другой, для дальнейшего сохранения и использования

        // перевод координат с серверных тут не нужен - это нужно на клиенте а не на сервере

        for (const ball of currentTask.balls) {
            ball.offsetPosition = { dx: ball.position.x, dy: ball.position.y };
            if (ball.path) {
                // fillZGaps будет внутри сглаживания.

                ball.offsetPath = ball.path.map(point => ({ dx: point.x, dy: point.y }));
            }
        }

        smoothenPathsForTaskBalls(currentTask);

        // здесь просто округляю координаты, т.к. Дима просил целые числа в результате.
        for (const ball of currentTask.balls) {
            if (ball.smoothedOffsetPath) {
                // smoothedPath не выставляется: координата z не привязана
                // ball.smoothedPath = ball.smoothedPath.map(point => ({ x: point.x.round(), y: point.y.round() }));
                ball.smoothedPathWithTimings = ball.smoothedPathWithTimings.map(point => ({ x: point.x.round(), y: point.y.round(), z: point.z }));

                ball.smoothedOffsetPath = ball.smoothedOffsetPath.map(point => ({ dx: point.dx.round(), dy: point.dy.round() }));
                ball.smoothedOffsetPathWithTimings = ball.smoothedOffsetPathWithTimings.map(point => ({ dx: point.dx.round(), dy: point.dy.round() }));
            }
        }
    }
}

export function calculateOffsetSegmentsLengths(offsetSector) {
    const result = [];
    for (let i = 1; i < offsetSector.length; i++) {
        result.add(utils.calculateOffsetsDistance(offsetSector[i - 1], offsetSector[i]));
    }
    return result;
}

/// генерирует точку на кривой, исходя из расстояния от начала этой кривой.
/// [smoothedOffsetSector] - кривая, на которой нужно генерировать точку
/// [smoothedSectorsLengths] - длины сегментов кривой.
/// [pointDistanceFromStart] - расстояние от начала кривой
export function generateOffsetForSmoothedLine(
    smoothedOffsetSector, smoothedSectorsLengths, pointDistanceFromStart) {
    // сначала нужно понять, в каком секторе будет находится точка.
    let prevSectorsLength = 0;
    let currentSectorLength = 0;
    for (let i = 0; i < smoothedSectorsLengths.length; i++) {
        currentSectorLength = smoothedSectorsLengths[i];
        if (prevSectorsLength + currentSectorLength >= pointDistanceFromStart) {
            // это нужная точка, нужно генерить
            const delta = (pointDistanceFromStart - prevSectorsLength) / currentSectorLength;
            const prevPoint = smoothedOffsetSector[i];
            const currentPoint = smoothedOffsetSector[i + 1];
            const generatedOffset = utils.generateSegmentOffset(prevPoint, currentPoint, delta);
            return generatedOffset;
        } else {
            prevSectorsLength += currentSectorLength;
        }
    }

    return smoothedOffsetSector.last();
}

/// List<Point> sector, List<Offset> smoothedOffsetSector
export function fillZGapsAndFillZForSmoothedSector(sector, smoothedOffsetSector) {
    let intermediateSector = sector.map((point) => _.clone(point));
    let resultSector = [sector.first()];

    // заполнив z-дырки, начальное и конечное расстояние-скорость будет считаться точнее и надежнее.
    fillZGapsSector(intermediateSector);
    // console.log(`sector ${sector.length} vs gaps-filled sector ${intermediateSector.length}`);
    const subsectorsWithSpeed = utils.calculateSubsectorsAndSpeedForPoints(intermediateSector);
    for (var i = 0; i < subsectorsWithSpeed.length; i++) {
        const [subsector, originalSpeed] = subsectorsWithSpeed[i];
        const subsectorOriginalLength = originalSpeed * (subsector.length - 1);
        // мне нужно сделать сейчас кусок сглаженного сектора, который будет начинаться в subsector.start, и заканчиваться в subsector.end.
        // для этого:
        // ищем это место начала, возвращаем его и индекс перед.
        // далее ищем место конца, возвращаем его и индекс... сразу после
        // как использовать: индекс после 1й точки, ...точки, индекс перед последней точкой. Считаем длину получившейся кривой.
        // если точка совпадает с точкой на сглаж. линии, то вернуть именно этот индекс.
        const [smoothedOffsetPointStart, startIndexesPair] =
            utils.calculateNearestPointAndIndexesOnOffsetsLine(smoothedOffsetSector, utils.pointToOffset(subsector.first()));
        const [smoothedOffsetPointEnd, endIndexesPair] =
            utils.calculateNearestPointAndIndexesOnOffsetsLine(smoothedOffsetSector, utils.pointToOffset(subsector.last()));

        // в теории: smoothedOffsetPointStart, точки smoothedOffsetSector, потом smoothedOffsetPointEnd.
        // если всё это в пределах одного отрезка smoothedOffsetSector, то все сложнее.
        // то есть если startIndexesPair[0]==endIndexesPair[0], то промежуточных точек нет.
        const smoothedSubsector = [smoothedOffsetPointStart];
        if (smoothedOffsetPointStart.dx != smoothedOffsetPointEnd.dx ||
            smoothedOffsetPointStart.dy != smoothedOffsetPointEnd.dy) {
            smoothedSubsector.addAll(smoothedOffsetSector.slice(
                startIndexesPair[1], endIndexesPair[0] + 1)); // +1: взять endIndexesPair[0] включительно
        }
        smoothedSubsector.add(smoothedOffsetPointEnd);

        const subsectorSmoothedLength = utils.calculateLineLengthByOffsets(smoothedSubsector);
        const speedDelta = subsectorSmoothedLength / subsectorOriginalLength;
        const newSpeed = originalSpeed * speedDelta;

        subsectorsWithSpeed[i] = [subsector, newSpeed];
    }
    const smoothedSectorsLengths = calculateOffsetSegmentsLengths(smoothedOffsetSector);

    let lastLineLengthFromSectorStart = 0;

    // теперь всего один вопрос: почему именно второй отрезок начинается не с начала, а со второго? Тогда как остальные начинаются с начала, как и должны?

    for (let subsectorIndex = 0; subsectorIndex < subsectorsWithSpeed.length; subsectorIndex++) {
        let [subsector, subsectorSpeed] = subsectorsWithSpeed[subsectorIndex];
        let subsectorSmoothedResult = [];
        // начиная со 2го элемента и до последнего ВКЛЮЧИТЕЛЬНО, добавляем расстояние (speed). генерим точку с z, добавляем. Начало сектора является концом предыдущего, поэтому разрывов не будет.
        // z можно взять из subsector, остальное генерить.
        for (let i = 1; i < subsector.length; i++) {
            lastLineLengthFromSectorStart += subsectorSpeed; //*1.1;
            // теперь нужно сгенерить точку.
            const generatedOffset =
                generateOffsetForSmoothedLine(smoothedOffsetSector, smoothedSectorsLengths, lastLineLengthFromSectorStart);
            const generatedPoint = { x: generatedOffset.dx, y: generatedOffset.dy };
            if (resultSector.last().x == generatedPoint.x && resultSector.last().y == generatedPoint.y) {
                console.log('Приехали. Точки на одном и том же месте');
            }

            generatedPoint.z = subsector[i].z;
            subsectorSmoothedResult.add(generatedPoint);
        }

        resultSector.addAll(subsectorSmoothedResult);
    }

    // console.log(`resultSector. amount must be the same ${resultSector.length}`);
    // после распределения, нужно молиться что я всё сделал правильно.
    return resultSector;
}

/// List<Point> sector
export function fillZGapsSector(sector) {
    // List<(int, List<Point>)> 
    const dataToFillGaps = [];
    // находим дырки и генерируем дополнительные кадры ВИДЕО. Это еще не кадры анимации
    for (let i = 1; i < sector.length; i++) {
        if (sector[i].z - sector[i - 1].z > 1) {
            // showDebug('gap (${zTimes[i - 1]}, ${zTimes[i]})');
            const gapStartPoint = sector[i - 1];
            const gapEndPoint = sector[i];
            // допустим, разница в z получилась 2. Тогда нужно сгенерить одну точку, на расстоянии 1/2 от gapStartPoint
            const gapDiff = gapEndPoint.z - gapStartPoint.z;
            const generatedGapPoints = [];
            // gapDiff==1 => ничего не генерится
            for (let gapIndex = 1; gapIndex <= gapDiff - 1; gapIndex++) {
                const generatedOffset =
                    utils.generateSegmentOffset(utils.pointToOffset(gapStartPoint), utils.pointToOffset(gapEndPoint), gapIndex / gapDiff);
                generatedGapPoints
                    .add({ x: generatedOffset.dx.round(), y: generatedOffset.dy.round(), z: gapStartPoint.z + gapIndex });
            }
            // showDebug(
            //     'gap (${resultSector[i - 1].z}, ${resultSector[i].z}), generatedPoints ${generatedGapPoints.map((point) => point.z)}');
            if (generatedGapPoints.isNotEmpty()) {
                dataToFillGaps.add([i, generatedGapPoints]);
            }
        }
    }

    // нужно учитывать кол-во вставленных. Индекс же меняется.
    let insertedCount = 0;
    for (const [idx, points] of dataToFillGaps) {
        sector.insertAll(idx + insertedCount, points);
        insertedCount += points.length;
    }
    // showDebug('ball path z ${resultSector.map((point)=>point.z)}');
    for (let i = 1; i < sector.length; i++) {
        if (sector[i].z - sector[i - 1].z > 1) {
            console.log('Все еще не идеально, ${sector[i - 1].z!}, ${sector[i].z!}');
        }
    }
}

export function smoothenSector(currentSectorToSmoothen) {
    // сначала обрабатываю отрезок, добавляю точки в случае если расстояние между точками слишком большое.
    // это нужно для более равномерного распределения точек. Так алгоритм сглаживания лучше работает.
    currentSectorToSmoothen = [...currentSectorToSmoothen]; // обязательно. Чтобы не бояться .add и тд
    currentSectorToSmoothen = utils.insertPointsWhenTooFar(currentSectorToSmoothen,
        imageBallRadiusServer * 1.5); //

    // c 20 и 50 еще можно поиграться, но вроде бы это нормальные параметры.
    currentSectorToSmoothen = opheim.smoothenLineOpheim(currentSectorToSmoothen, 20, 50);

    return currentSectorToSmoothen;
}

export function smoothenPathsForTaskBalls(task) {
    const ballsWithPath = task.balls.filter((ball) => ball.offsetPath != null);

    for (const currentBall of ballsWithPath) {
        const offsetPath = currentBall.offsetPath;
        const path = currentBall.path;

        if (currentBall.deflectionSectors == null) {
            // на выходе: line1: (startIndex 1, endIndex 3), line2: (startIndex 4, endIndex 5). В любом случае, line1.endIndex должно быть == line2.startIndex, а это не так
            const deflectionSectors = utils.calculateLineDeflectionSectors(offsetPath, path);
            currentBall.deflectionSectors = deflectionSectors;
            // int insertedPointsCount = 0;
            let startIndexToProcess = 0;
            let smoothedOffsetPath = [];
            let smoothedPathWithTimings = [];
            // берем отрезок от начала до очередного сектора
            for (let sectorIndex = 0; sectorIndex < deflectionSectors.length; sectorIndex++) {
                const sector = deflectionSectors[sectorIndex];
                const startIndexToCut = startIndexToProcess;
                const endIndexToCut = sector.line1.endIndex;
                const currentSectorToSmoothen =
                    offsetPath.slice(startIndexToCut, endIndexToCut + 1); // +1, и выходит "включая" от start до end

                // sectorToSmoothen (и по крайней мере path), предыдущий конец должен совпадать с текущим началом. Индекс точно.
                const currentPathSectorToSmoothen = path.slice(startIndexToCut, endIndexToCut + 1);
                if (sectorIndex > 0) {
                    currentSectorToSmoothen.insert(0, deflectionSectors[sectorIndex - 1].intersectionPoint);
                }
                // и в конце добавить точку пересечения. Она точно не будет null, так как null значения отсекаются на предыдущем этапе.
                // в currentSectorToSmoothen нужно в начало добавить предыдущий sector.intersectionPoint.
                // Не точку линии, а именно intersectionPoint, если она есть. Лишнее потом удалю
                currentSectorToSmoothen.add(sector.intersectionPoint);
                const smoothedSector = smoothenSector(currentSectorToSmoothen, task.fps);
                // FIXME smoothedPathSector здесь длиной 4, а должно быть 5
                const smoothedPathSector = fillZGapsAndFillZForSmoothedSector(currentPathSectorToSmoothen, smoothedSector);
                // точка перес. от предыдущего сектора тут не нужна. Но если предыдущего сектора нет, то первая точка не является точкой конца пред. сектора
                smoothedOffsetPath.addAll(smoothedSector);
                smoothedPathWithTimings.addAll(smoothedPathSector);

                startIndexToProcess = endIndexToCut;
            }
            // в конце обрабатываем последний отрезок: берем путь от последнего сектора (lastSector.line2.startIndex) и до конца (параметр можно опустить).
            // откуда берется lastProcessedIndex 187???
            const startIndexToCut = startIndexToProcess;
            const currentSectorToSmoothen = offsetPath.slice(startIndexToCut);
            const currentPathSectorToSmoothen = path.slice(startIndexToCut);
            if (deflectionSectors.isNotEmpty()) {
                currentSectorToSmoothen.insert(0, deflectionSectors.last().intersectionPoint);
            }
            // console.log(
            //     'Последний отрезок: ${startIndexToCut}, ${offsetPath.length}, ${currentSectorToSmoothen.length}, ${deflectionSectors.last.angle}, ${deflectionSectors.last.intersectionPoint}');
            const smoothedSector = smoothenSector(currentSectorToSmoothen, task.fps);
            const smoothedPathSector = fillZGapsAndFillZForSmoothedSector(currentPathSectorToSmoothen, smoothedSector);
            smoothedPathWithTimings.addAll(smoothedPathSector);
            smoothedOffsetPath.addAll(smoothedSector);
            // и теперь просто пройтись и убрать повторные.
            {
                const duplicateIndexesToCut = [];
                for (let i = 1; i < smoothedOffsetPath.length; i++) {
                    const prev = smoothedOffsetPath[i - 1];
                    const curr = smoothedOffsetPath[i];
                    if (prev.dx == curr.dx && prev.dy == curr.dy) {
                        duplicateIndexesToCut.add(i - 1);
                    }
                }
                if (duplicateIndexesToCut.isNotEmpty()) {
                    const newOffsetPath = [];
                    for (let i = 0; i < smoothedOffsetPath.length; i++) {
                        if (!duplicateIndexesToCut.includes(i)) {
                            newOffsetPath.add(smoothedOffsetPath[i]);
                        }
                    }
                    smoothedOffsetPath = newOffsetPath;
                }
            }

            {
                const duplicateIndexesToCut = [];
                for (let i = 1; i < smoothedPathWithTimings.length; i++) {
                    const prev = smoothedPathWithTimings[i - 1];
                    const curr = smoothedPathWithTimings[i];
                    if (prev.z == curr.z) {
                        duplicateIndexesToCut.add(i - 1);
                    }
                }
                if (duplicateIndexesToCut.isNotEmpty()) {
                    const newPointPath = [];
                    for (let i = 0; i < smoothedPathWithTimings.length; i++) {
                        if (!duplicateIndexesToCut.includes(i)) {
                            newPointPath.add(smoothedPathWithTimings[i]);
                        }
                    }
                    smoothedPathWithTimings = newPointPath;
                }
            }

            currentBall.smoothedOffsetPath = smoothedOffsetPath;
            currentBall.smoothedPathWithTimings = smoothedPathWithTimings;
            currentBall.smoothedOffsetPathWithTimings = smoothedPathWithTimings.map((p) => utils.pointToOffset(p));
            // console.log('path ${currentBall.path!.length} vs smoothedPath ${currentBall.smoothedPathWithTimings!.length}');

            // проблема в самом последнем индексе. z 202 и 210. В оригинальном path так же. Нужно проверить вычисление последнего сектора.
            for (let i = 1; i < smoothedPathWithTimings.length; i++) {
                if (smoothedPathWithTimings[i].z - smoothedPathWithTimings[i - 1].z != 1) {
                    console.log('Ошибка, что-то не так с z');
                }
            }
            console.log('конец');
        }
    }
}