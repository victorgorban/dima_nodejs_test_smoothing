import _ from 'lodash';
import { serverCanvasWidth, serverCanvasHeight, imageBallRadiusServer } from './constants.js';
import './extensions.js';

export function setTaskMinAndMaxTimeZ(task) {
    for (const ball of task.balls) {
        if (!ball.path) continue;
        let ballPath = ball.path;
        if (ballPath.isEmpty()) continue;

        let ballMinTimeZ = 9999;
        let ballMaxTimeZ = 0;

        if (ballMaxTimeZ == 0) {
            ballMaxTimeZ = 1;
        }

        if (ballPath.first().z && ballPath.first().z < ballMinTimeZ) {
            ballMinTimeZ = ballPath.first().z;
        }
        if (ballPath.last().z && ballPath.last().z > ballMaxTimeZ) {
            ballMaxTimeZ = ballPath.last().z;
        }

        ball.minTimeZ = ballMinTimeZ;
        ball.maxTimeZ = ballMaxTimeZ;
    }

    let ballsWithTimeZ = task.balls.filter((ball) => ball.maxTimeZ);

    task.minTimeZ = _.min(ballsWithTimeZ.map((ball) => ball.minTimeZ));
    task.maxTimeZ = _.min(ballsWithTimeZ.map((ball) => ball.maxTimeZ));
}

export function fillZGaps(ball) {
    const dataToFillGaps = [];
    // находим дырки и генерируем дополнительные кадры ВИДЕО. Это еще не кадры анимации
    for (let i = 1; i < ball.path.length; i++) {
        if (ball.path[i].z - ball.path[i - 1].z > 1) {
            const gapStartPoint = ball.path[i - 1];
            const gapEndPoint = ball.path[i];
            // допустим, разница в z получилась 2. Тогда нужно сгенерить одну точку, на расстоянии 1/2 от gapStartPoint
            const gapDiff = gapEndPoint.z - gapStartPoint.z;
            const generatedGapPoints = [];
            // gapDiff==1 => ничего не генерится
            for (let gapIndex = 1; gapIndex <= gapDiff - 1; gapIndex++) {
                const generatedPoint =
                    generateSegmentPoint(gapStartPoint, gapEndPoint, gapIndex / gapDiff);
                generatedPoint.z = gapStartPoint.z + gapIndex;
                generatedGapPoints
                    .add(generatedPoint);
            }
            // showDebug(
            //     'gap (${ball.path![i - 1].z}, ${ball.path![i].z}), generatedPoints ${generatedGapPoints.map((point) => point.z)}');
            if (generatedGapPoints.isNotEmpty()) {
                dataToFillGaps.add([i, generatedGapPoints]);
            }
        }
    }

    // нужно учитывать кол-во вставленных. Индекс же меняется.
    let insertedCount = 0;
    for (const [idx, points] of dataToFillGaps) {
        ball.path.insertAll(idx + insertedCount, points);
        insertedCount += points.length;
    }
}


export function generateSegmentOffset(begin, end, segmentCoefficient) {
    const xa = begin.dx;
    const xb = end.dx;
    const ya = begin.dy;
    const yb = end.dy;
    const xc = xa + (xb - xa) * segmentCoefficient;
    const yc = ya + (yb - ya) * segmentCoefficient;

    return { dx: xc, dy: yc };
}

export function generateSegmentPoint(begin, end, segmentCoefficient) {
    const xa = begin.x;
    const xb = end.x;
    const ya = begin.y;
    const yb = end.y;
    const xc = xa + (xb - xa) * segmentCoefficient;
    const yc = ya + (yb - ya) * segmentCoefficient;

    return { x: xc, y: yc };
}


export function calculateLineLengthByPoints(points) {
    let result = 0;
    if (points.length < 2) return result;

    for (let i = 1; i < points.length; i++) {
        const prevPoint = points[i - 1];
        const currentPoint = points[i];
        result += calculatePointsDistance(prevPoint, currentPoint);
    }

    return result;
}

export function calculateLineLengthByOffsets(points) {
    let result = 0;
    if (points.length < 2) return result;

    for (let i = 1; i < points.length; i++) {
        const prevPoint = points[i - 1];
        const currentPoint = points[i];
        result += calculateOffsetsDistance(prevPoint, currentPoint);
    }

    return result;
}

export function getNearestOffsetOnLine(lineStart, lineEnd, point) {
    // first convert line to normalized unit vector
    let lineVector = calculateOffsetsVectorGuide(lineStart, lineEnd);
    let dx = lineVector.dx;
    let dy = lineVector.dy;
    let mag = (dx.sqr() + dy.sqr()).sqrt();
    dx /= mag;
    dy /= mag;

    // translate the point and get the dot product
    let lambda = (dx * (point.dx - lineStart.dx)) + (dy * (point.dy - lineStart.dy));
    let x4 = (dx * lambda) + lineStart.dx;
    let y4 = (dy * lambda) + lineStart.dy;
    return { dx: x4, dy: y4 };
}

export function getNearestPointOnLine(lineStart, lineEnd, point) {
    // first convert line to normalized unit vector
    let lineVector = calculatePointsVectorGuide(lineStart, lineEnd);
    let vx = lineVector.x;
    let vy = lineVector.y;
    let mag = (vx.sqr() + vy.sqr()).sqrt();
    vx /= mag;
    vy /= mag;

    // translate the point and get the dot product
    let lambda = (vx * (point.x - lineStart.x)) + (vy * (point.y - lineStart.y));
    let x4 = (vx * lambda) + lineStart.x;
    let y4 = (vy * lambda) + lineStart.y;
    return { x: x4, y: y4, z: point.z };
}

export function calculateOffsetsDistance(p1, p2) {
    if (p1.dx == p2.dx && p1.dy == p2.dy) return 0;

    const squareX = Math.pow(p2.dx - p1.dx, 2);
    const squareY = Math.pow(p2.dy - p1.dy, 2);
    return Math.sqrt(squareX + squareY);
}

export function calculatePointsDistance(p1, p2) {
    if (p1.x == p2.x && p1.y == p2.y) return 0;

    const squareX = Math.pow(p2.x - p1.x, 2);
    const squareY = Math.pow(p2.y - p1.y, 2);
    return Math.sqrt(squareX + squareY);
}

function degrees(radians) {
    // Store the value of pi.
    var pi = Math.PI;
    // Multiply radians by 180 divided by pi to convert to degrees.
    return radians * (180 / pi);
}

export function calculateLineDistance(points) {
    let result = 0.0;
    if (points.length < 2) return result;
    for (let i = 1; i < points.length; i++) {
        result += calculateOffsetsDistance(points[i - 1], points[i]);
    }

    return result;
}

/// получает одну из вершин прямоугольного треугольника, по известным двум вершинам и известному катету.
/// Храни Господь этого пацана https://github.com/gusenov/problem-solving-in-pascal/blob/master/ua/06-find-third-vertex-of-triangle/FindThirdVertexOfTriangle.pas
// Прямоугольный треугольник:
//   A
//   |\
//   |α\
//   |  \
// b |   \ с - гипотенуза
//   |    \
//   |     \
//   |      \
//   |γ=90° β\
//   |________\
//   C   a   B
/// Принято обозначать вершину прямого угла буквой C, а гипотенузу — c.
/// Катеты обозначаются a и b, а величины противолежащих им углов — α и β соответственно.
export function getOrtogonalTrianglePointA(
    pointB, pointC, basementLengthCB, perpendicularLengthAC, direction) {
    // Единичный вектор:
    let vdx = (pointB.x - pointC.x) / basementLengthCB;
    let vdy = (pointB.y - pointC.y) / basementLengthCB;
    let unitVec = { x: vdx, y: vdy };
    let rdx = 0, rdy = 0;

    if (direction == true) {
        // первое решение.
        rdx = pointC.x + (-unitVec.y * perpendicularLengthAC);
        rdy = pointC.y + (unitVec.x * perpendicularLengthAC);
    } else {
        // второе решение.
        rdx = pointC.x + (unitVec.y * perpendicularLengthAC);
        rdy = pointC.y + (-unitVec.x * perpendicularLengthAC);
    }
    return { x: rdx, y: rdy };
}

// результат: A,B,C из Ax + By + C = 0 
export function generateLineKoefficientsBySegment(lineStart, lineEnd) {
    // Канонич уравнение прямой: (x-x1)/(x2-x1) = (y-y1)/(y2-y1);
    // допустим, (x2-x1) это dx, а (y2-y1) это dy
    // A=dx, B=-dy, C=x2*y1-x1*y2

    let x1 = lineStart.x, y1 = lineStart.y;
    let x2 = lineEnd.x, y2 = lineEnd.y;

    let A = y2 - y1, B = x1 - x2;
    let C = x2 * y1 - x1 * y2;
    return [A, B, C]
}

export function calculateDistanceFromLineToPoint(point, lineStart, lineEnd) {
    // Ax + By + C = 0, point (a,b) (x,y)
    // d = |(a*x0+b*y0+c)| / sqrt(A^2 + B^2)

    let [lineA, lineB, lineC] = generateLineKoefficientsBySegment(lineStart, lineEnd);

    let d = Math.abs(lineA * point.x + lineB * point.y + lineC) / (lineA.sqr() + lineB.sqr()).sqrt()

    return d;
}

export function calculateCircleAndLineIntersectionPoints(center, r, lineStart, lineEnd) {
    // (x-x0).sqr() + (y-y0).sqr() = r.sqr()
    // 
    let { x: x0, y: y0 } = center;
    let [A, B, C] = generateLineKoefficientsBySegment(lineStart, lineEnd);
    let distanceToLine = calculateDistanceFromLineToPoint(center, lineStart, lineEnd);
    if (distanceToLine > r) return [];

    // если <=, то работает формула.
    // для формулы нужно чтобы B!=0
    // {(x−x0)2+(y−y0)2=r2
    // Ax+By+C=0
    // y = -(Ax+C)/B - нужно найти x.
    // https://math.stackexchange.com/a/3166304

    if (Math.abs(B) > 0.001) {
        // a*x^2+b*x+c=0, где
        // a=A^2+B^2
        // b=2*A*C+2*A*B*y0−2*B^2*x0
        // c=C^2+2*B*C*y0−B^2*(r^2−x0^2−y0^2)
        let a = A.sqr() + B.sqr();
        let b = 2 * A * C + 2 * A * B * y0 - 2 * B.sqr() * x0;
        let c = C.sqr() + 2 * B * C * y0 - B.sqr() * (r.sqr() - x0.sqr() - y0.sqr())

        let [x1, x2] = solveQuadraticEquasion(a, b, c)
        let y1 = -(A * x1 + C) / B;
        let y2 = -(A * x2 + C) / B;
        return [{ x: x1, y: y1 }, { x: x2, y: y2 }]
    } else
    // если B очень мал, то просто игнорим его, просто подставляем в формулу окружности x=-C/A
    {
        let x = -C / A;
        // a*y^2+b*y+c=0
        // a=A^2+B^2
        // b=2*B*C+2*A*B*x0−2*A^2*y0
        // c=C^2+2*A*C*x0−A^2*(r^2−x0^2−y0^2)

        let a = A.sqr() + B.sqr();
        let b = 2 * B * C + 2 * A * B * x0 - 2 * A.sqr() * y0
        let c = C.sqr() + 2 * A * C * x0 - A.sqr() * (r.sqr() - x0.sqr() - y0.sqr())

        let [y1, y2] = solveQuadraticEquasion(a, b, c);
        return [{ x: x, y: y1 }, { x: x, y: y2 }]
    }

}

// решение квадратного уравнения по дискриминанту. a!=0;
export function solveQuadraticEquasion(a, b, c) {
    // x = (-b +-sqr(Disc))/2*a, где Disc = b^2-4*a*c
    let disc = b.sqr() - 4 * a * c;
    let x1 = (-b + disc.sqrt()) / (2 * a);
    let x2 = (-b - disc.sqrt()) / (2 * a);
    return [x1, x2]
}

/// находит вектор прямой по 2м точкам отрезка. По сути это разница координат, которые можно подставить в канон. уравнение прямой
export function calculateOffsetsVectorGuide(p1, p2) {
    return { dx: p2.dx - p1.dx, dy: p2.dy - p1.dy };
}

export function calculatePointsVectorGuide(p1, p2) {
    return { x: p2.x - p1.x, y: p2.y - p1.y };
}

export function calculateNearestSideIntersectionPointWithDistance(startPoint, endPoint) {
    let side1 = { start: { x: imageBallRadiusServer, y: imageBallRadiusServer }, end: { x: serverCanvasWidth - imageBallRadiusServer, y: imageBallRadiusServer } }
    let side2 = { start: { x: serverCanvasWidth - imageBallRadiusServer, y: imageBallRadiusServer }, end: { x: serverCanvasWidth - imageBallRadiusServer, y: serverCanvasHeight - imageBallRadiusServer } }
    let side3 = { start: { x: serverCanvasWidth - imageBallRadiusServer, y: serverCanvasHeight - imageBallRadiusServer }, end: { x: imageBallRadiusServer, y: serverCanvasHeight - imageBallRadiusServer } }
    let side4 = { start: { x: imageBallRadiusServer, y: serverCanvasHeight - imageBallRadiusServer }, end: { x: imageBallRadiusServer, y: imageBallRadiusServer } }

    // по каждой линии: проверить что линии пересекаются. Если пересекаются, то посчитать точку пересечения линий.
    // если точка пересечения находится внутри поля, то сохранить её. Потом из всех выбрать ту, которая ближе всех к endPoint.
    // 
    let intersectionPoints = [];
    for (let tableSide of [side1, side2, side3, side4]) {
        // для корректного считания углов, нужно чтобы линии расходились, а не сходились.
        // поэтому если endPoint ближе к sideStart, чем к sideEnd, то нужно поменять sideStart и sideEnd.
        let { start: sideStart, end: sideEnd } = tableSide;
        if (calculatePointsDistance(endPoint, sideStart) < calculatePointsDistance(endPoint, sideEnd)) {
            [sideStart, sideEnd] = [sideEnd, sideStart]
        }

        let angleBetweenLines = calculatePointsAngleDeg(sideStart, sideEnd, startPoint, endPoint)
        let angleDegreesTolerance = 1;
        if (angleBetweenLines - angleDegreesTolerance < 0 || angleBetweenLines + angleDegreesTolerance > 180) {
            continue;
        }
        let intersectionPoint = calculateLinesIntersectionPoint(sideStart, sideEnd, startPoint, endPoint);
        if (!intersectionPoint) { continue; }
        // 1. если точка находится "сзади" направления, то игнорить её. Определение "сзади": по к startPoint она ближе чем к endPoint.
        if (calculatePointsDistance(startPoint, intersectionPoint) < calculatePointsDistance(endPoint, intersectionPoint)) {
            continue;
        }
        if (intersectionPoint.x < 0 || intersectionPoint.y < 0 || intersectionPoint.x > serverCanvasWidth || intersectionPoint.y > serverCanvasHeight) {
            continue;
        }
        // 2. если точка находится не в пределах стола, то игнорить её.
        // 3. только если точка на столе (0, max), только тогда её сохранить.

        intersectionPoints.push(intersectionPoint);
    }
    // [{point, distance}]
    let intersectionPointsWithDistance = []
    for (let point of intersectionPoints) {
        let distance = calculatePointsDistance(point, endPoint);
        intersectionPointsWithDistance.push({ point, distance });
    }
    intersectionPointsWithDistance = _.sortBy(intersectionPointsWithDistance, 'distance');
    return intersectionPointsWithDistance[0]
}

// линии должны быть расходящимися.
export function calculatePointsAngleDeg(line1Start, line1End, line2Start, line2End) {
    // v1, v2 - направляющие векторы прямых. Фактически это разница между координатами точек. Используется в записи уравнений.
    let v1 = calculatePointsVectorGuide(line1Start, line1End);
    let v2 = calculatePointsVectorGuide(line2Start, line2End);

    const cosA =
        (v1.x * v2.x + v1.y * v2.y) / ((v1.x.sqr() + v1.y.sqr()).sqrt() * (v2.x.sqr() + v2.y.sqr()).sqrt());

    // косинус может быть меньше чем -1 из-за ошибок округления, а это невозможно в математике. Поэтому приходится округлять.
    let angleRad = Math.acos(cosA.roundDecimals(5));
    // тут же могу посчитать их точку пересечения, если угол не 0
    let angleDeg = degrees(angleRad);
    angleDeg = angleDeg.roundDecimals(5);

    return angleDeg;
}

// здесь не проверяется сама возможность пересечения. Эта возможность должна быть проверена раньше.
export function calculateLinesIntersectionPoint(line1Start, line1End, line2Start, line2End) {
    let v1 = calculatePointsVectorGuide(line1Start, line1End);
    let v2 = calculatePointsVectorGuide(line2Start, line2End);

    const cosA =
        (v1.x * v2.x + v1.y * v2.y) / ((v1.x.sqr() + v1.y.sqr()).sqrt() * (v2.x.sqr() + v2.y.sqr()).sqrt());

    // косинус может быть меньше чем -1 из-за ошибок округления, а это невозможно в математике. Поэтому приходится округлять.
    let angleRad = Math.acos(cosA.roundDecimals(5));
    // тут же могу посчитать их точку пересечения, если угол не 0
    let angleDeg = degrees(angleRad);
    angleDeg = angleDeg.roundDecimals(5);

    let intersectionPoint;
    // console.log('Угол ${angleDeg.round()}, $line2Start, $line2End');
    // Угол между прямыми найден. Теперь найти точку пересечения (в случае если ответ не 0 и не 180, с учетом округления).

    let t;
    if (v2.x != 0 && v2.y != 0)
    // нормальная ситуация, когда вторая линия не имеет нулей в напр. векторе, и первая линия может иметь максимум один 0 в напр векторе.
    // один 0 в напр. векторе означает, что линия параллельна одной из осей.
    {
        t = (v2.x * (line1Start.y - line2Start.y) - v2.y * (line1Start.x - line2Start.x)) /
            (v1.x * v2.y - v1.y * v2.x);
    } else if (v2.x == 0 && v1.x != 0) {
        t = (line2Start.x - line1Start.x) / v1.x;
    } else if (v2.y == 0 && v1.y != 0) {
        t = (line2Start.y - line1Start.y) / v1.y;
    } else
    // Общих точек пересечения нет. Такого не должно быть. Скорее всего, у нас одна из линий с нулевым вектором. Возвращаем null
    {
        return null;
    }
    // т.к. вектор может быть частично с 0, делить на него нельзя. Нужно умножить. Тем более это пропорция.

    const x1 = v1.x * t + line1Start.x;
    // final x2 = v2.x * t + line2Start.x;

    const y1 = v1.y * t + line1Start.y;
    // final y2 = v2.y * t + line2Start.y;

    // console.log('Пересечение: ($x1, $y1)');
    intersectionPoint = { x: x1, y: y1 };

    return intersectionPoint
}

/// считает угол между отрезками, возвращает угол 0 <= 180 и точку пересечения. Прямые должны быть не-нулевой длины. Отрезки должны идти как бы друг за другом, например начало второго может совпадать с концом первого.
// Offset line1Start, Offset line1End, Offset line2Start, Offset line2End, double minAngle, double maxAngle
export function calculateAngleDegAndIntersectionSequental(
    line1Start, line1End, line2Start, line2End, minAngle, maxAngle) {
    // line1 изначально входит в точку, а line2 из нее выходит. Для считания угла нужно так, чтобы они обе выходили из точки.
    [line1End, line1Start] = [line1Start, line1End];

    // v1, v2 - направляющие векторы прямых. Фактически это разница между координатами точек. Используется в записи уравнений.
    let v1 = calculateOffsetsVectorGuide(line1Start, line1End);
    let v2 = calculateOffsetsVectorGuide(line2Start, line2End);

    const cosA =
        (v1.dx * v2.dx + v1.dy * v2.dy) / ((v1.dx.sqr() + v1.dy.sqr()).sqrt() * (v2.dx.sqr() + v2.dy.sqr()).sqrt());

    // косинус может быть меньше чем -1 из-за ошибок округления, а это невозможно в математике. Поэтому приходится округлять.
    let angleRad = Math.acos(cosA.roundDecimals(5));
    // тут же могу посчитать их точку пересечения, если угол не 0
    let angleDeg = degrees(angleRad);
    angleDeg = angleDeg.roundDecimals(5);

    let intersectionPoint;
    // console.log('Угол ${angleDeg.round()}, $line2Start, $line2End');
    if (angleDeg >= minAngle && angleDeg != 180) {
        // сэкономлю немного вычислений для слишком мягких углов.
        if (angleDeg < maxAngle) {
            // Угол между прямыми найден. Теперь найти точку пересечения (в случае если ответ не 0 и не 180, с учетом округления).

            let t;
            if (v2.dx != 0 && v2.dy != 0)
            // нормальная ситуация, когда вторая линия не имеет нулей в напр. векторе, и первая линия может иметь максимум один 0 в напр векторе.
            // один 0 в напр. векторе означает, что линия параллельна одной из осей.
            {
                t = (v2.dx * (line1Start.dy - line2Start.dy) - v2.dy * (line1Start.dx - line2Start.dx)) /
                    (v1.dx * v2.dy - v1.dy * v2.dx);
            } else if (v2.dx == 0 && v1.dx != 0) {
                t = (line2Start.dx - line1Start.dx) / v1.dx;
            } else if (v2.dy == 0 && v1.dy != 0) {
                t = (line2Start.dy - line1Start.dy) / v1.dy;
            } else
            // Общих точек пересечения нет. Такого не должно быть. Скорее всего, у нас одна из линий с нулевым вектором. Возвращаем 0 и null
            {
                return [0, null];
            }
            // т.к. вектор может быть частично с 0, делить на него нельзя. Нужно умножить. Тем более это пропорция.

            const x1 = v1.dx * t + line1Start.dx;
            // final x2 = v2.dx * t + line2Start.dx;

            const y1 = v1.dy * t + line1Start.dy;
            // final y2 = v2.dy * t + line2Start.dy;

            // console.log('Пересечение: ($x1, $y1)');
            intersectionPoint = { dx: x1, dy: y1 };

            // final xRoot = LUSolver(matrix: , knownValues: [line1Start.dx, line2Start.dx]);
        }
    } else
    // если угол 0, это не ошибка. Это значит что просто ударили в борт или шар так, что биток откатился точно назад, такое бывает.
    // тогда возвращаем 0 (чем меньше angleDeg, тем резче угол), а точкой пересечения считаю среднее между концом 1 линии и началом 2й.
    // т.к. в начале функции первая линия развернута, то берем не конец 1й линии, а начало.
    // 0 значит что шар откатился назад. 180 значит что шар покатился дальше.
    {
        if (angleDeg < minAngle)
        // для точного разворота на 180 не нужно вычислять доли градуса, т.к. может быть погрешность. Выставляем минимальный угол, а потом просто возьмем среднюю точку из этих
        {
            angleDeg = 0;
        }
        intersectionPoint = generateSegmentOffset(line1Start, line2Start, 0.5);
    }

    if (intersectionPoint != null) {
        let dx = intersectionPoint.dx;
        let dy = intersectionPoint.dy;

        // todo обработать intersectionPoint. Если она за пределами поля, то подкорректировать координаты так, чтобы они были внутри поля.
        if (dx < imageBallRadiusServer) {
            dx = imageBallRadiusServer;
        }
        if (dy < imageBallRadiusServer) {
            dy = imageBallRadiusServer;
        }
        if (dx > serverCanvasWidth - imageBallRadiusServer) {
            dx = serverCanvasWidth - imageBallRadiusServer;
        }
        if (dy > serverCanvasHeight - imageBallRadiusServer) {
            dy = serverCanvasHeight - imageBallRadiusServer;
        }
        intersectionPoint = { dx, dy };
    }

    return [angleDeg, intersectionPoint];
}

// class LineSector {
//   LineSector({required this.start, required this.end, this.startIndex = 0, this.endIndex = 0, this.lineLength = 0});
//   Offset start;
//   int startIndex;
//   int endIndex;
//   Offset end;
//   double lineLength;

//   LineSector clone() {
//     return LineSector(end: end, start: start, endIndex: endIndex, startIndex: startIndex, lineLength: lineLength);
//   }
// }

// typedef SectorWithAngle = ({LineSector line1, LineSector line2, double angle, Offset? intersectionPoint});

/// Вычисление точек преломления
export function calculateLineDeflectionSectors(offsetPath, path) {
    let prevSector = { startIndex: 0, endIndex: 0, start: offsetPath.first(), end: offsetPath.first(), lineLength: 0 };
    let currentSector = { startIndex: 0, endIndex: 0, start: offsetPath.first(), end: offsetPath.first(), lineLength: 0 };
    // minSectorLength это просто диаметр шара.
    // todo minSectorLength нужно вычислять исходя из расстояния между точками и taskFps
    let minSectorLength = imageBallRadiusServer * 2;
    // странно что с *2 и даже 1.5 работает неверно. Видимо я что-то перепутал. Но хотя бы с *1 работает.
    const maxSectorInternalAngle = 155; // т.е. maxSectorInternalAngle и острее/меньше уже считается преломлением.
    const minSectorInternalAngle = 10;
    let sectorsWithAngle = [];
    const deflectionSectors = [];

    for (let i = 1; i < offsetPath.length; i++) {
        const currentPointDistance = calculateOffsetsDistance(offsetPath[i - 1], offsetPath[i]);
        // если длине текущего сектора не хватает длины, значит просто добавляем точку в конец.
        currentSector.end = offsetPath[i];
        currentSector.endIndex = i;
        currentSector.lineLength += currentPointDistance;
        if (currentSector.lineLength < minSectorLength) {
            // todo в currentSector надо добавлять текущую точку. И увеличивать lineLength, индексы и тд. Вроде это и так делается, строчкой выше.
            // ничего, продолжаем
        } else {
            // если длина текущего сектора и без этого достаточно длинная, то нужно проверить первый отрезок, и пересчитать длину от второй до последней.
            const firstPointDistance =
                calculateOffsetsDistance(offsetPath[currentSector.startIndex], offsetPath[currentSector.startIndex + 1]);

            // если длина от второй точки все еще достаточна, то попытаться добавить точку к prevSector.

            if (currentSector.lineLength - firstPointDistance >= minSectorLength) {
                // prevPointDistance: это гэп (отрезок) между концом prev и началом current.
                const prevPointDistance = prevSector.endIndex == 0
                    ? 0
                    : calculateOffsetsDistance(offsetPath[prevSector.endIndex], offsetPath[prevSector.endIndex + 1]);

                // prevSector должен быть на расстоянии одного отрезка от текущего отрезка. Если отрезки идут четко друг за другом, то их точка пересечения будет являться концом и началом. Толку в этом нет.
                prevSector.endIndex = prevSector.endIndex + 1;
                prevSector.end = offsetPath[prevSector.endIndex];
                prevSector.lineLength += prevPointDistance;

                currentSector.startIndex = currentSector.startIndex + 1;
                currentSector.start = offsetPath[currentSector.startIndex];
                currentSector.lineLength -=
                    calculateOffsetsDistance(offsetPath[currentSector.startIndex - 1], offsetPath[currentSector.startIndex]);

                // проверяю что currentSector.startIndex и prevSector.endIndex отличается на один и не более.
                // Если индексы совпадают, то такое должно быть только для prevSector.endIndex 0.
            }
        }

        // если и предыдущий отрезок теперь достаточно длинный, то можно попробовать его укоротить, на счет последней точки
        if (prevSector.lineLength >= minSectorLength) {
            const firstPointDistance =
                calculateOffsetsDistance(offsetPath[prevSector.startIndex], offsetPath[prevSector.startIndex + 1]);
            if (prevSector.lineLength - firstPointDistance >= minSectorLength) {
                prevSector.start = offsetPath[prevSector.startIndex + 1];
                prevSector.startIndex = prevSector.startIndex + 1;

                prevSector.lineLength -= firstPointDistance;
            }
        }

        // теперь посчитать угол. Если угол резче минимального, то начать последовательность. Когда угол станет снова мягче минимального, последовательность прекратить.
        // Затем найти угловой сектор с самым жестким углом. Там будет точка пересечения, это примерная точка где шар повернул.

        // то есть считаем угол между prevSector и currentSector.
        // Если угол резче минимального, то добавляем эту инфу в sectorsWithAngle.
        if (prevSector.lineLength >= minSectorLength && currentSector.lineLength >= minSectorLength) {
            let [angleDeg, intersectionPoint] = calculateAngleDegAndIntersectionSequental(prevSector.start, prevSector.end,
                currentSector.start, currentSector.end, minSectorInternalAngle, maxSectorInternalAngle);
            // если угол еще достаточно острый, и при этом более острый, чем 1/2 от остроты max
            const minSectorsWithAngleAngle =
                sectorsWithAngle.isEmpty() ? NaN : _.min(sectorsWithAngle.map((s) => s.angle));
            if (angleDeg <= maxSectorInternalAngle &&
                (isNaN(minSectorsWithAngleAngle) || (angleDeg - minSectorsWithAngleAngle) < 10)) {
                // позволяю максимум 30град отклонения от минимального угла. Это чтобы быстрее выходить из вычислений угла.
                sectorsWithAngle.add({
                    angle: angleDeg,
                    line1: _.clone(prevSector),
                    line2: _.clone(currentSector),
                    intersectionPoint: intersectionPoint
                });
            } else
                // если угол был достаточно резким, но он УЖЕ недостаточно резкий, то это можно обработать.
                if (sectorsWithAngle.isNotEmpty()) {
                    // находим минимальный угол (т.е. самый резкий). minBy вернет null для пустого массива, но у меня он не пустой.
                    // minBy не подходит: sectorsWithAngle могут иметь несколько минимальных углов. В этом случае мне нужно взять срединный индекс.
                    const minAngle = _.min(sectorsWithAngle.map((sector) => sector.angle));
                    const sectorsWithMinAngle = sectorsWithAngle.filter((sector) => sector.angle == minAngle);
                    let sectorWithSharpestAngle;
                    if (sectorsWithMinAngle.length < 2) {
                        sectorWithSharpestAngle = sectorsWithMinAngle.first();
                    } else {
                        // взять средний элемент
                        sectorWithSharpestAngle = sectorsWithMinAngle[(sectorsWithMinAngle.length / 2).ceil() -
                            1]; // -1 потому что round() и ceil() могут дать индекс 3 из 3. Но никогда не даст индекс 0 из 3.
                    }

                    prevSector.startIndex = sectorWithSharpestAngle.line2.startIndex;
                    prevSector.endIndex = sectorWithSharpestAngle.line2.endIndex;
                    prevSector.start = offsetPath[prevSector.startIndex];
                    prevSector.end = offsetPath[prevSector.endIndex];
                    prevSector.lineLength =
                        calculateLineLengthByOffsets(offsetPath.slice(prevSector.startIndex, prevSector.endIndex + 1));
                    if (prevSector.endIndex + (prevSector.endIndex - prevSector.startIndex) < offsetPath.length) {
                        currentSector.startIndex = prevSector.endIndex;
                        currentSector.endIndex = currentSector.startIndex + (prevSector.endIndex - prevSector.startIndex);
                        currentSector.lineLength =
                            calculateLineLengthByOffsets(offsetPath.slice(currentSector.startIndex, currentSector.endIndex + 1));
                        i = currentSector.endIndex;
                    }
                    // не забыть посчитать длину линий

                    sectorWithSharpestAngle.intersectionPointWithZ = path[sectorWithSharpestAngle.line2.startIndex];

                    deflectionSectors
                        .add(sectorWithSharpestAngle); // тут проверить что line1.endIndex почти совпадает с line2.startIndex
                    sectorsWithAngle = [];
                }
        }
    }
    if (deflectionSectors.isNotEmpty()) {
        console.log(
            `deflectionSectors: ${deflectionSectors.length}, ${deflectionSectors.map((sector) => sector.angle.roundDecimals(3))}`);
        for (const sector of deflectionSectors) {
            // console.log(
            //     `deflectionSector lines, (${sector.line1.startIndex}, ${sector.line1.endIndex}), (${sector.line2.startIndex}, ${sector.line2.endIndex})`);
        }
    }

    return deflectionSectors;
}


/// вставляет промежуточные точки, если расстояние между исходными точками больше [targetDistance].
/// количество необходимых точек округляется в большую сторону, если отклонение (в разах) больше [xTolerance].
export function insertPointsWhenTooFar(offsetList, targetDistance, xTolerance = 0.3) {
    if (offsetList.length < 2) return offsetList;
    // int insertedPoints = 0;
    const result = [offsetList.first()];

    for (let i = 1; i < offsetList.length; i++) {
        const prevPoint = offsetList[i - 1];
        const currentPoint = offsetList[i];
        const neighborsDistance = calculateOffsetsDistance(prevPoint, currentPoint);
        if (neighborsDistance > targetDistance) {
            const requiredAdditionalPointsCount = (neighborsDistance / targetDistance - xTolerance).ceil() - 1;
            // здесь нужно именно <=requiredAdditionalPointsCount + 1, чтобы сгенерировалась также последняя точка.
            for (let j = 1; j <= requiredAdditionalPointsCount + 1; j++) {
                const generatedPoint = generateSegmentOffset(
                    prevPoint,
                    currentPoint,
                    j /
                    (requiredAdditionalPointsCount +
                        1)); // +1, потому что если нужно добавить 1 точку, то она должна быть на 1/2 отрезка.
                result.add(generatedPoint);

                // insertedPoints++;
            }
        }
        // а эта точка добавляется только тогда, когда расстояние от result.last до currentPoint >= targetDistance
        // дополнительные точки генерятся между prevPoint и currentPoint. Поэтому "закрываем" сгенеренные точки мы именно currentPoint, не prev.
        if (calculateOffsetsDistance(result.last(), currentPoint) >= targetDistance * (1 - xTolerance)) {
            result.add(currentPoint);
        }
        // FIXME сейчас меня волнует линия от первой точки пересечения до offsetPath[offsetPath.length-2]
    }
    if (result.last() != offsetList.last()) {
        result.add(offsetList.last());
    }

    return result;
}

export function pointToOffset(p) {
    return { dx: p.x, dy: p.y };
}

export function offsetToPoint(p) {
    return { x: p.dx, y: p.dy };
}

// List<(List<Point>, double)>
export function calculateSubsectorsAndSpeedForPoints(sector) {
    if (sector.length <= 15) {
        // один подсектор
        const length = calculateLineLengthByPoints(sector) / (sector.length - 1);
        return [[sector, length]];
    } else if (sector.length >= 16 && sector.length <= 30) {
        // два подсектора
        const sector1EndIndex = (sector.length / 2).floor();
        const sector1 =
            sector.slice(0, sector1EndIndex + 1); // +1 - чтобы включить последнюю точку, т.е. последний отрезок
        const sector1Length = calculateLineLengthByPoints(sector1) / (sector1.length - 1);
        const sector2 = sector.slice(sector1EndIndex);
        const sector2Length = calculateLineLengthByPoints(sector2) / (sector2.length - 1);
        return [[sector1, sector1Length], [sector2, sector2Length]];
    } else if (sector.length >= 31 && sector.length <= 60) {
        // 3 подсектора
        const sector1EndIndex = (sector.length / 3 * 1).floor();
        const sector2EndIndex = (sector.length / 3 * 2).floor();
        const sector1 = sector.slice(0, sector1EndIndex + 1);
        const sector1Length =
            calculateLineLengthByPoints(sector1) / (sector1.length - 1); // делим на количество отрезков, не на кол-во точек
        const sector2 = sector.slice(sector1EndIndex, sector2EndIndex + 1);
        const sector2Length = calculateLineLengthByPoints(sector2) / (sector2.length - 1);
        const sector3 = sector.slice(sector2EndIndex);
        const sector3Length = calculateLineLengthByPoints(sector3) / (sector3.length - 1);

        return [[sector1, sector1Length], [sector2, sector2Length], [sector3, sector3Length]];
    } else if (sector.length >= 61 && sector.length <= 100) {
        // 4 подсектора
        const sector1EndIndex = (sector.length / 4 * 1).floor();
        const sector2EndIndex = (sector.length / 4 * 2).floor();
        const sector3EndIndex = (sector.length / 4 * 3).floor();
        const sector1 = sector.slice(0, sector1EndIndex + 1);
        const sector1Length = calculateLineLengthByPoints(sector1) / (sector1.length - 1);
        const sector2 = sector.slice(sector1EndIndex, sector2EndIndex + 1);
        const sector2Length = calculateLineLengthByPoints(sector2) / (sector2.length - 1);
        const sector3 = sector.slice(sector2EndIndex, sector3EndIndex + 1);
        const sector3Length = calculateLineLengthByPoints(sector3) / (sector3.length - 1);
        const sector4 = sector.slice(sector3EndIndex);
        const sector4Length = calculateLineLengthByPoints(sector4) / (sector4.length - 1);

        return [[sector1, sector1Length], [sector2, sector2Length], [sector3, sector3Length], [sector4, sector4Length]];
    } else {
        // 5 подсекторов
        const sector1EndIndex = (sector.length / 5 * 1).floor();
        const sector2EndIndex = (sector.length / 5 * 2).floor();
        const sector3EndIndex = (sector.length / 5 * 3).floor();
        const sector4EndIndex = (sector.length / 5 * 4).floor();
        const sector1 = sector.slice(0, sector1EndIndex + 1);
        const sector1Length = calculateLineLengthByPoints(sector1) / (sector1.length - 1);
        const sector2 = sector.slice(sector1EndIndex, sector2EndIndex + 1);
        const sector2Length = calculateLineLengthByPoints(sector2) / (sector2.length - 1);
        const sector3 = sector.slice(sector2EndIndex, sector3EndIndex + 1);
        const sector3Length = calculateLineLengthByPoints(sector3) / (sector3.length - 1);
        const sector4 = sector.slice(sector3EndIndex, sector4EndIndex + 1);
        const sector4Length = calculateLineLengthByPoints(sector4) / (sector4.length - 1);
        const sector5 = sector.slice(sector4EndIndex);
        const sector5Length = calculateLineLengthByPoints(sector5) / (sector5.length - 1);

        return [
            [sector1, sector1Length],
            [sector2, sector2Length],
            [sector3, sector3Length],
            [sector4, sector4Length],
            [sector5, sector5Length]
        ];
    }
}

// (Offset, (int, int)) calculateNearestPointAndIndexesOnOffsetsLine(List<Offset> offsetLine, Offset point) {
export function calculateNearestPointAndIndexesOnOffsetsLine(offsetLine, point) {
    let firstPointDistance = calculateOffsetsDistance(offsetLine.first(), point);
    let lastPointDistance = calculateOffsetsDistance(offsetLine.last(), point);

    if (firstPointDistance == 0) {
        return [point, [0, 0]];
    } else if (lastPointDistance == 0) {
        return [point, [offsetLine.length - 1, offsetLine.length - 1]];
    } else if (offsetLine.length == 2) {
        const nearestPoint = getNearestOffsetOnLine(offsetLine.first(), offsetLine.last(), point);
        return [nearestPoint, [0, 1]];
    }

    // List<(int, double)>
    let distancesByIndex = [];

    for (let i = 0; i < offsetLine.length; i++) {
        const currentPointDistance = calculateOffsetsDistance(offsetLine[i], point);
        if (currentPointDistance == 0) {
            return [offsetLine[i], [i, i]];
        }
        distancesByIndex.add([i, currentPointDistance]);
    }

    // теперь понять, как это фильтровать. Найду первый попавшийся с минимальной дистанцией. Теперь проверю prev, current и next.
    const minDistanceWithIndex = _.minBy(distancesByIndex, (p0) => p0[1]);
    const minDistanceIndex = minDistanceWithIndex[0];
    const minDistancePoint = offsetLine[minDistanceIndex];
    if (minDistanceIndex == 0) {
        const nearestPoint = getNearestOffsetOnLine(offsetLine[0], offsetLine[1], point);
        return [nearestPoint, [0, 1]];
    } else if (minDistanceIndex == offsetLine.length - 1) {
        const nearestPoint =
            getNearestOffsetOnLine(offsetLine[offsetLine.length - 2], offsetLine[offsetLine.length - 1], point);
        return [nearestPoint, [offsetLine.length - 2, offsetLine.length - 1]];
    } else {
        const prevPointDistanceByIndex = distancesByIndex[minDistanceIndex - 1];
        const nextPointDistanceByIndex = distancesByIndex[minDistanceIndex + 1];
        if (prevPointDistanceByIndex[1] <= nextPointDistanceByIndex[1]) {
            const prevPoint = offsetLine[prevPointDistanceByIndex[0]];
            const nearestPoint = getNearestOffsetOnLine(prevPoint, minDistancePoint, point);
            return [nearestPoint, [prevPointDistanceByIndex[0], minDistanceIndex]];
        } else {
            const nextPoint = offsetLine[nextPointDistanceByIndex[0]];
            const nearestPoint = getNearestOffsetOnLine(minDistancePoint, nextPoint, point);
            return [nearestPoint, [minDistanceIndex, nextPointDistanceByIndex[0]]];
        }
    }
}

export function checkIsPointInsideTetragon(pointToCheck, tetragon) {
    if (tetragon.length != 4) {
        throw new Error('Кол-во точек в коридоре должно быть ровно 4');
    }

    let triangle1 = [tetragon[0], tetragon[1], tetragon[2]];
    let triangle2 = [tetragon[2], tetragon[3], tetragon[0]];

    return checkIsPointInsideTriangle(pointToCheck, triangle1) || checkIsPointInsideTriangle(pointToCheck, triangle2);
}

export function checkIsPointInsideTriangle(pointToCheck, trianglePoints) {
    let { x: x0, y: y0 } = pointToCheck;
    let [p1, p2, p3] = trianglePoints;
    let { x: x1, y: y1 } = p1;
    let { x: x2, y: y2 } = p2;
    let { x: x3, y: y3 } = p3;

    let result1 = (x1 - x0) * (y2 - y1) - (x2 - x1) * (y1 - y0);
    let result2 = (x2 - x0) * (y3 - y2) - (x3 - x2) * (y2 - y0);
    let result3 = (x3 - x0) * (y1 - y3) - (x1 - x3) * (y3 - y0);

    // если находится на границе
    if (result1 == 0 || result2 == 0 || result3 == 0) return true;

    // если внутри, то все 3 должны быть одинакового знака
    if (result1 < 0 && result2 < 0 && result3 < 0) return true;
    if (result1 > 0 && result2 > 0 && result3 > 0) return true;

    return false;
}