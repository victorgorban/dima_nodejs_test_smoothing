import _ from 'lodash';
import * as utils from './utils.js';
import { serverCanvasWidth, serverCanvasHeight, imageBallRadiusServer } from './constants.js';
import './extensions.js';

// выставляет поле classesTrajectories
// для начала нужно определить отрезки.
// Изначально можно определить только начальные позиции шаров (и порядок начала движения).
export function detectShotClasses(shot) {
    for (let pointData of shot.pointsCloud) {
        delete pointData.class;
    }
    // на выходе хорошо бы иметь разбиение по классам ({1: [...]}), и в них также сгенерированные точки.

    let { dataWithoutStatic, lastStaticPoints, lastStaticPointsFromEnd, lastStaticPointsFullyStatic, lastStaticPointsFromEndFullyStatic } = detectPointsStartsAndDeleteStaticFromBeginAndEnd(shot);
    if (!lastStaticPoints?.length) return;
    lastStaticPoints = _.sortBy(lastStaticPoints, 'frame');
    // последняя статическая точка все-таки нужна, как минимум для определения соударения.
    dataWithoutStatic.push(...lastStaticPoints)
    dataWithoutStatic = _.uniq(dataWithoutStatic);
    dataWithoutStatic = _.sortBy(dataWithoutStatic, 'frame')
    // теперь в dataWithoutStatic у меня где-то половина исходных точек, т.е. очищенные от статических как с начала, так и с конца.
    // то есть теперь можно работать только с динамикой, остальное игнорировать.

    // 0: [pointData, pointData]
    let shotPointsDataClassified = {};
    let sortedLastStaticPoints = _.sortBy(lastStaticPoints, 'frame');
    for (let i of Object.keys(sortedLastStaticPoints)) {
        sortedLastStaticPoints[i].class = i;
        shotPointsDataClassified[i] = [sortedLastStaticPoints[i]];
    }
    // теперь проходим по lastStaticPoints, и смотрим у кого z меньше. Это будет биток, т.к. именно биток начинает двигаться первым.
    let firstStaticPointDataToMove = sortedLastStaticPoints[0];
    let minFrameNumber = firstStaticPointDataToMove.frame;
    let maxFrameNumber = dataWithoutStatic.last().frame;
    let groupedByFrame = _.groupBy(dataWithoutStatic, 'frame');

    // теперь, когда есть данные без статики (т.е. динамические), можно начать искать ближайшую (по расстоянию) динамическую точку среди них. 
    // по 2м точкам можно уже определить направление (хоть и неточно), далее сделать коридор направления, и искать ближайшнее вхождение в него.
    // если проходим рядом со статической точкой (идем в неё по вектору направления, радиус шара*2, или даже *2.2 (2.5?) с учетом погрешности), 
    // то нужно проверить наличие касания. Касание можно обнаружить только наличием второй динамической точки на каком-то следующем кадре, 
    // или (если повезет) тем что в какой-то момент есть lastStaticPoints с текущим z.
    // при том что кадры из-за багов могут быть не все, поэтому иногда придется строить виртуальные точки (исходя из прошлой пары z-точек), 
    // и от этой виртуальной точки (и по направлению) искать ближайшую. А может и не надо, может быть хватит обычного направления и коридора.
    // основной алгоритм в цикле - проверка точек с нужным z (их пара штук) на попадание в нужную область. 
    // Или 4-угольник, или круг, или сектор круга (круг и сектор). То есть 1 функция с опц. параметрами (круг, 4х-угольник)
    // Обязательно проверять все точки в z, попасть могут несколько - сложные случаи бывают. Если так, то проверить отклонение от направления и вернуть более подходящую.
    // проще всего будет играть от целевого шара, т.к. начальная траектория у него понятная.
    // Потому что биток по сути может пойти куда угодно, если не знать точную скорость и вращение.

    // это должно быть для каждого шара из первого кадра, или как?
    // прошлая точка для начала. Точка направления: то есть от неё идет линия до currentPointData, и вот по этой линии можно искать следующую точку.

    // определение направления битка. Пока нет соударения, здесь будет только один шар не-статичный в кадре
    let ball0StartPointData = firstStaticPointDataToMove;
    let ball0DirectionPointData = detectBall0DirectionPointData(minFrameNumber, maxFrameNumber, groupedByFrame, ball0StartPointData);
    // {
    //     let ball0Points = [];
    //     let detectionResult = detectBall0DirectionPoints(sortedLastStaticPoints, dataWithoutStatic, ball0StartPointData)
    //     ball0DirectionPointData = detectionResult.directionEndPoint;
    //     ball0Points = detectionResult.foundPointsData;
    //     for (let pd of ball0Points) {
    //         pd.class = 0;
    //     }
    //     for (let i = ball0Points.length - 2; i >= 0; i--) {
    //         ball0StartPointData = ball0Points[i];
    //         if (utils.calculatePointsDistance(ball0StartPointData.point, ball0DirectionPointData.point) < imageBallRadiusServer) {
    //             continue;
    //         } else {
    //             break;
    //         }
    //     }

    //     shotPointsDataClassified[0].push(...ball0Points);
    // }

    if (!ball0DirectionPointData) { return; }
    ball0DirectionPointData.class = 0;
    shotPointsDataClassified[ball0DirectionPointData.class].push(ball0DirectionPointData);
    // определение углов: с учетом дикой погрешности, целевой шар может пойти не просто в 90град секторе, а все 180. 
    // если в этом секторе находятся сразу >1 точки на кадре, то выбираем из неё ближайшую к предположительной траектории.
    // в любом случае нужно проанализировать сразу несколько кадров (хотя бы 5, а лучше 10, до тех пор пока направляющая не уперлась в борт),
    // или следующий шар не начал движение.  
    // и если 2 шара, то построить 2 расходящиеся прямые, от статики к найденному. 
    // Предыдущие кадры (где 1 шар) отнести к одной из этих прямых по принципу угла к направляющей. 
    // Можно вообще забить даже на секторы, и просто смотреть на близость к направляющей (угол между направляющей, и прямой статика - найденная точка).


    let ball0Result = findAllDeflectionPointsOnLine({
        currentFrameNumber: ball0DirectionPointData.frame,
        groupedFrames: groupedByFrame,
        lastStaticPointsFromStart: lastStaticPoints,
        lastStaticPointsFromEnd: lastStaticPointsFromEnd,
        startPointData: ball0StartPointData,
        directionPointData: ball0DirectionPointData,
        pointsDataClassified: shotPointsDataClassified,
        thisClassNumber: 0,
        excludeLineIndexes: []
    })
    let busyLineIndexes = [0];

    for (let lineIndex of Object.keys(shotPointsDataClassified)) {
        if (lineIndex == 0) continue;
        // if (lineIndex == 2) continue;
        let classPointsData = shotPointsDataClassified[lineIndex]
        let startPointData = classPointsData[0];
        let directionPointData = classPointsData[1];
        if (!startPointData || !directionPointData) continue;

        findAllDeflectionPointsOnLine({
            currentFrameNumber: directionPointData.frame,
            groupedFrames: groupedByFrame,
            lastStaticPointsFromStart: lastStaticPoints,
            lastStaticPointsFromEnd: lastStaticPointsFromEnd,
            startPointData: startPointData,
            directionPointData: directionPointData,
            pointsDataClassified: shotPointsDataClassified,
            thisClassNumber: lineIndex,
            excludeLineIndexes: busyLineIndexes
        })
        busyLineIndexes.push(lineIndex);
    }


    // todo надо пройти по всем начальным и конечным точкам. Нужно проставить поле class для статической части. 
    // Если там есть класс, то проставить этот  класс всем точкам рядом (только если поля class нет у точки)
    // если нет класса (для статики), то проставить класс. Плюс надо бы иметь некую среднюю точку для статических групп.
    for (let midStaticPoint of [...lastStaticPoints, ...lastStaticPointsFromEnd, ...lastStaticPointsFullyStatic]) {
        let classNumber = midStaticPoint.class;
        if (classNumber == undefined) {
            classNumber = +(_.last(lastStaticPoints).class) + 1;
        }
        if (midStaticPoint.class == undefined) {
            shotPointsDataClassified[classNumber] = []
        }
        let pointsDataNearThisPointWithoutClass = shot.pointsCloud.filter(pd => pd.class == undefined && utils.calculatePointsDistance(midStaticPoint.point, pd.point) <= imageBallRadiusServer * 2)
        for (let pd of pointsDataNearThisPointWithoutClass) {
            pd.class = classNumber;
        }
        shotPointsDataClassified[classNumber].unshift(...pointsDataNearThisPointWithoutClass);
    }

    // плюс еще конец траекторий, тоже надо проставлять.
    for (let [key, values] of Object.entries(shotPointsDataClassified)) {
        shotPointsDataClassified[key] = _.uniq(values)
    }

    shot.pointsCloud = _.uniq(shot.pointsCloud)
    for (let pd of shot.pointsCloud) {
        pd.class = +pd.class;
    }

    // в classes у нас кадры начала движения.
    let classes = [];
    for (let lastStaticPoint of lastStaticPoints) {
        classes.push({ class: lastStaticPoint.class, frame: lastStaticPoint.point.z + 1 })
    }

    for (let lastStaticPoint of lastStaticPointsFullyStatic) {
        classes.push({ class: lastStaticPoint.class })
    }
    shot.classes = classes;

    console.log('all done?'); // похоже что после соударения с шаром, продолжение пути битка не фиксируется почему-то.
    return [shot.classes, shot.pointsCloud, shotPointsDataClassified];
}


// что тут происходит: идем по направлению линии, ищем точки. 
// Если попадаем на z, где находится одна из lastStaticPoints, то готовимся к обработке соударения.
// Если направление идет в борт, и (имея последнюю скорость (с учетом погрешности в +- 25%), 
// следующий кадр с учетом виртуальных точек должен быть уже на борте (расстояние 1R от борта) или дальше (за бортом) ), то
// 1. точка реально может быть на борте (узкий коридор шириной в 2R, есть точка со след. Z). Найти её, и просчитать точку перес. уже на след. кадре.
// 2. точка уже отразилась, и находится на отражении от борта. Тогда в любом случае нужно построить точку удара о борт (по начальному направлению), 
// и найти точку в новом коридоре (нормальном коридоре, не узком).
// 3. важное: на малых скоростях (скорость вычисляется как среднее за пред. несколько кадров), 
// идея направления почти не имеет смысла - кадр может сильно дергаться. 
// 4. найденную точку пересечения или добавляем в текущую pointsDataClassified[n] (сгенер. точку с дробным z, это для соударения с шаром или с бортом), 
// или (для соударения с шаром), добавляем найденную следующую точку в pointsDataClassified[n+1]. 
// То есть соударение с новым шаром создает новую запись для этого нового шара: pointsDataClassified[n+1]. 
// Другие линии: последовательно берем pointsDataClassified (без учета уже обработанных), и также делаем findAllDeflectionPointsOnLine. 
// В этом случае лучше просто в радиусе искать (без учета направления даже, радиус 2 пред. скорости)
// виртуальные точки не корректировать, этим займется постобработка.

// еще раз, определение 2х точек (кто есть кто): 1. грубо определяю точку соударения. 2. провожу направляющую от точки соударения к центру статич. шара. 
// 3. жду когда будет 2 динам. точки в кадре, банально круг увеличиваю. Беру за целевую ту, у которой угол между направляющей меньше. 
// (если точка как бы в обратную сторону от направляющей, то угол должен быть максимальный) Если только 1а точка до самого борта, то я хз.
// а определение следующей точки на линии (без преломления) - это просто поиск в коридоре, коридор идет от startPoint по направлению directionPoint. Назад идти не может абсолютно.
/** [currentFrameNumber] - искать нужно точки фрейма currentFrameNumber+1; Не всегда совпадает с [directionPointData], т.к. могут быть виртуальные точки и тд.
 * [pointsDataClassified] - общий объект с уже классифицированными точками.
 * [processedClassNumbers] - номера классов (предположительно, уже обработанные линии), точки которых будут игнорироваться при обработке точек соударения.
 * 
 * */
function findAllDeflectionPointsOnLine({
    currentFrameNumber, groupedFrames, lastStaticPointsFromStart, lastStaticPointsFromEnd,
    startPointData, directionPointData, pointsDataClassified, thisClassNumber, processedClassNumbers = [] }) {
    let minFrameNumber = currentFrameNumber + 1;
    let maxFrameNumber = _.maxBy(lastStaticPointsFromEnd, 'frame').frame;
    let pointsDataClassifiedToIgnore = {}
    for (let processedNumber of processedClassNumbers) {
        pointsDataClassifiedToIgnore[processedNumber] = pointsDataClassified[processedNumber];
    }
    let pointsDataClassifiedToProcess = _.clone(pointsDataClassified);
    for (let processedNumber of Object.keys(pointsDataClassifiedToIgnore)) {
        delete pointsDataClassifiedToProcess[processedNumber];
    }

    // let groupedByFrameToProcess = _.groupBy(Object.values(pointsDataClassifiedToProcess).reduce((all, current) => [...all, ...current], []), 'frame');
    let groupedByFrameToProcess = groupedFrames;
    let framesCountWithoutFoundDirectionPoint = 0;
    // идем по кадрам. Если на следующем кадре начинается движение другого шара (т.е. мы ударили его), то обрабатываем эту ситуацию.
    // иначе обрабатываем логику продолжения траектории текущего шара
    for (currentFrameNumber = minFrameNumber; currentFrameNumber <= maxFrameNumber; currentFrameNumber++) {
        // console.log('currentFrameNumber to process', currentFrameNumber)
        let distanceBetweenLastPoints = utils.calculatePointsDistance(startPointData.point, directionPointData.point);
        if (directionPointData.frame - startPointData.frame > 1) {
            // distanceBetweenLastPoints /= directionPointData.frame - startPointData.frame
        }

        let framePointsDataToProcess = groupedByFrameToProcess[currentFrameNumber];
        if (!framePointsDataToProcess?.length) continue;
        // если есть точка/точки в lastStaticPointsFromStart с текущим frame, то берем точку с минимальным углом отклонения или ближайшую
        // нужно изначально пронумеровать статич. точки. Проставить classNumber. По принципу "кто раньше двинется"
        // до нужного фрейма, похоже, вообще не доходят. При том что фрейм должен быть 100%, т.к. lastStaticPoint в этой точке есть.
        let frameLastStaticPoints = lastStaticPointsFromStart.filter(pd => pd.frame == currentFrameNumber);
        if (!frameLastStaticPoints.length) {
            // просто ищем следующую точку по направлению, может быть после удара о борт
            // 1. найти точку без учета борта, просто по направлению
            // 2. определить, рядом ли мы с бортом (в плане направления)
            // 3. если рядом (и в коридоре к борту нет точки), то строим точку соударения с бортом, а потом ищем точку просто по расстоянию. 
            // Направление тут не построить.
            let distanceBetweenStartAndDirectionPoints = utils.calculatePointsDistance(startPointData.point, directionPointData.point);
            let directionCorridorWidth = imageBallRadiusServer * 4; // сразу берем широкий коридор
            let directionCorridorLength = imageBallRadiusServer + distanceBetweenLastPoints * 2 + distanceBetweenLastPoints * framesCountWithoutFoundDirectionPoint;
            // по направлению от startPoint до directionPoint

            // начало коридора - это directionPoint, то есть направляющая. Предыдущая точка не нужна точно.
            let corridorStartPoint1 = utils.getOrtogonalTrianglePointA(startPointData.point, directionPointData.point, distanceBetweenStartAndDirectionPoints, directionCorridorWidth, true)
            let corridorStartPoint2 = utils.getOrtogonalTrianglePointA(startPointData.point, directionPointData.point, distanceBetweenStartAndDirectionPoints, directionCorridorWidth, false)

            let corridorDirectionSegmentKoefficient = (directionCorridorLength + distanceBetweenStartAndDirectionPoints) / distanceBetweenStartAndDirectionPoints;
            let corridorDirectionEndPoint = utils.generateSegmentPoint(startPointData.point, directionPointData.point, corridorDirectionSegmentKoefficient);
            let corridorEndPoint1 = utils.getOrtogonalTrianglePointA(directionPointData.point, corridorDirectionEndPoint, directionCorridorLength, directionCorridorWidth, true)
            let corridorEndPoint2 = utils.getOrtogonalTrianglePointA(directionPointData.point, corridorDirectionEndPoint, directionCorridorLength, directionCorridorWidth, false)
            let corridor = [corridorEndPoint1, corridorEndPoint2, corridorStartPoint2, corridorStartPoint1];

            let insideCorridorPointsData = []; // проверить точки на вхождение в этот коридор. Если их >1, то берем ближайшую по направлению
            // todo если distanceBetweenLastPoints слишком мала (допустим, 5 единиц), то нет смысла искать по направлению, 
            // как минимум в поиск следующей точки текущего класса. Искать можно только по расстоянию, т.е. радиусу. 
            if (distanceBetweenLastPoints <= imageBallRadiusServer * 2) {
                let minFrameNumberToSearchForNextPoint = currentFrameNumber;
                let maxFrameNumberToSearchForNextPoint = currentFrameNumber + 5;
                for (currentFrameNumber = minFrameNumberToSearchForNextPoint; currentFrameNumber <= maxFrameNumberToSearchForNextPoint; currentFrameNumber++) {
                    framePointsDataToProcess = groupedByFrameToProcess[currentFrameNumber];
                    if (!framePointsDataToProcess?.length) continue;
                    let radiusToSearchForPoint = imageBallRadiusServer * 4 + (currentFrameNumber - minFrameNumberToSearchForNextPoint) * distanceBetweenLastPoints;
                    insideCorridorPointsData = framePointsDataToProcess.filter(pd => utils.calculatePointsDistance(pd.point, directionPointData.point) <= radiusToSearchForPoint);
                    if (insideCorridorPointsData.length) {
                        break;
                    }
                }
            } else {
                for (let pointDataToProcess of framePointsDataToProcess) {
                    if (utils.checkIsPointInsideTetragon(pointDataToProcess.point, corridor)) {
                        insideCorridorPointsData.push(pointDataToProcess);
                    }
                }
            }

            if (insideCorridorPointsData.length) {
                // я надеюсь, здесь точка 1. Если больше 1, значит нужную (это след. точка текущего шара) 
                // вычисляем по близости от directionPointData. Углы здесь смысла не имеют.
                let pointsWithDistance = insideCorridorPointsData.map(pd => ({ pointData: pd, distance: utils.calculatePointsDistance(pd.point, directionPointData.point) }));
                pointsWithDistance = _.sortBy(pointsWithDistance, 'distance')
                let thisBallNextPointData = pointsWithDistance[0].pointData;
                thisBallNextPointData.class = thisClassNumber;
                pointsDataClassified[thisBallNextPointData.class].push(thisBallNextPointData)
                // несколько точек друг на друге, во время движения - это бич исходных данных.
                if (utils.calculatePointsDistance(thisBallNextPointData.point, directionPointData.point) < imageBallRadiusServer) {
                    continue;
                }
                startPointData = directionPointData;
                directionPointData = thisBallNextPointData;
            } else {
                let { point: nearestSideIntersectionPoint, distance: nearestSideIntersectionPointDistance }
                    = (utils.calculateNearestSideIntersectionPointWithDistance(directionPointData.point, corridorDirectionEndPoint) || {}); // функция, которая выясняет что направленное расстояние до одного из бортов меньше чем параметр. 
                // достаточно будет определить, что сам коридор выходит за границы стола.
                // И в идеале еще находит точку касания борта. Это может быть логика с углами, где угол между 2мя линиями, и точка перес-я, если несть.
                // прогнать по 4м прямым, вот и всё.
                if (nearestSideIntersectionPoint && nearestSideIntersectionPointDistance < directionCorridorLength) {
                    // от точки касания борта, ищем точку просто по расстоянию.
                    // расстояние (радиус для поиска) увеличивается с каждым безуспешным кадром, т.е. это кол-во кадров тоже надо где-то вычислять или хранить.
                    // походу придется делать подцикл. Максимум 5 итераций. Не забыть пропущенные кадры добавить
                    let nearestSideIntersectionPointData = { ...directionPointData, type: 'virtual', point: nearestSideIntersectionPoint };
                    pointsDataClassified[thisClassNumber].push(nearestSideIntersectionPointData)
                    let minFrameNumberToSearchForNextPoint = currentFrameNumber;
                    let maxFrameNumberToSearchForNextPoint = currentFrameNumber + 5;
                    for (currentFrameNumber = minFrameNumberToSearchForNextPoint; currentFrameNumber <= maxFrameNumberToSearchForNextPoint; currentFrameNumber++) {
                        framePointsDataToProcess = groupedByFrameToProcess[currentFrameNumber];
                        if (!framePointsDataToProcess?.length) continue;
                        let radiusToSearchForPointNearTableSide = imageBallRadiusServer * 4 + distanceBetweenLastPoints * 2 + (currentFrameNumber - minFrameNumberToSearchForNextPoint) * distanceBetweenLastPoints;
                        let framePointsDataInsideRadiusWithDistance = framePointsDataToProcess.filter(pd => utils.calculatePointsDistance(pd.point, nearestSideIntersectionPoint) <= radiusToSearchForPointNearTableSide)
                            .map(pd => ({ pointData: pd, distance: utils.calculatePointsDistance(pd.point, nearestSideIntersectionPoint) }));
                        if (framePointsDataInsideRadiusWithDistance.length) {
                            framePointsDataInsideRadiusWithDistance = _.sortBy(framePointsDataInsideRadiusWithDistance, 'distance')
                            let thisBallNextPointData = framePointsDataInsideRadiusWithDistance[0].pointData;
                            thisBallNextPointData.class = thisClassNumber;
                            pointsDataClassified[thisBallNextPointData.class].push(thisBallNextPointData)
                            // несколько точек друг на друге (на соседних фреймах), во время движения - это бич исходных данных.
                            if (utils.calculatePointsDistance(thisBallNextPointData.point, nearestSideIntersectionPointData.point) < imageBallRadiusServer) {
                                continue;
                            }
                            startPointData = nearestSideIntersectionPointData;
                            directionPointData = thisBallNextPointData;
                            // currentFrameNumber сам увеличивается в цикле
                            break;
                        }
                    }


                }

            }


        } else
        // есть соударение в следующем (или в этом, может быть между фреймами) фрейме. Ищем соударение шаров и их траектории в последующих кадрах.
        {
            // если получилось так, что несколько шаров начали двигаться в одном кадре, то выбираем из них ближайшего.
            let [unused, staticPointData] = calculateLowestDistanceForPointsData(directionPointData, frameLastStaticPoints)
            // теперь смотрим на точку пересечения линии и круга, и по ней предполагаем куда полетел целевой шар. 
            // Там довольно прямая линия, при этом биток может пойти вообще куда угодно, и по любой траектории - закрутка и погрешность направления всё портит.
            // находим целевой шар, добавляем в соотв. массив (с нужным классом)
            // "другой" шар это биток.

            // todo проблема в точке пересечения - это малое расстояние между startPoint и directionPoint. 
            // Из-за этого вектор направления может быть куда угодно.
            // R*2.5 потому, что погрешность. В идеальных условиях должно быть 2.0
            let [point1, point2] = utils.calculateCircleAndLineIntersectionPoints(staticPointData.point, imageBallRadiusServer * 2.5, startPointData.point, directionPointData.point)
            // иногда (при фактически касании, из-за погрешности) может быть пустой массив. Тогда нужно взять ближайшую точку к линии, это будет касание.
            if (!point1) {
                point1 = point2 = utils.getNearestPointOnLine(startPointData.point, directionPointData.point, staticPointData.point);
            }
            // берем ближайшую точку пересечения круга.
            let [unused2, deflectionPoint] = calculateLowestDistanceForPointsData(directionPointData, [{ point: point1 }, { point: point2 }]);
            deflectionPoint = deflectionPoint.point;
            if (utils.calculatePointsDistance(deflectionPoint, staticPointData.point) > imageBallRadiusServer * 5) {
                continue; // > 5 радиусов от положения точки до точки пересечения? Точно ошибка данных.
            }

            let deflectionPointData = { ...directionPointData, point: deflectionPoint, type: 'virtual' };
            pointsDataClassifiedToProcess[thisClassNumber].push(deflectionPointData);
            // pointsDataClassifiedToProcess[thisClassNumber].push({ point: point1, type: 'virtual' }, { point: point2, type: 'virtual' });


            // следующую точку целевого шара нужно обязательно найти ДО борта (вставлять здесь рекурсивный поиск по грубому направлению - затея без особого смысла).
            // и также ДО соударения со следующим шаром (в данном радиусе). Я просто это ограничение не обрабатываю здесь.
            // направление для поиска целевого шара: от deflectionPointData до staticPointData - строим коридор от staticPointData и дальше. 
            // При этом оба шара должны найтись в опр. радиусе (на каждый кадр это не может быть больше чем 2 пред. расстояния).
            /** Поиск следующей соударения с другим шаром */
            {
                // поиск точки/точек. Т.к. динамич. точек в кадре может быть больше чем надо, надо игнорировать те, что уже обработаны: 
                // игнорировать точки, которые есть в pointsDataClassifiedToIgnore
                // начальный кадр: текущий +1;
                // конечный возможный кадр: maxFrameNumber-1. Но если есть точка в lastStaticPointsFromStart, которую не нужно игнорить, и которая дальше начального кадра, то именно она-1.
                // начальный радиус: distanceBetweenLastPoints*2. Конечный радиус: startRadius+distanceBetweenLastPoints*10, 
                // радиус увеличивается на distanceBetweenLastPoints с каждым кадром, где не найдены 2 точки в радиусе.
                // Расстояние у коридора: радиус.
                // Конечное расстояние у коридора: направленное расстояние от staticPoint до борта, не наоборот.
                // начальная ширина коридора: r шара *2 (в каждую сторону). Конечное расстояние: r шара на 3, потом r шара на 4. 
                // Менять ширину в каждом кадре, где найдены >=2 шара в нужном радиусе.
                let startFrameToSearchForPoint = currentFrameNumber + 1; // первый динамический кадр
                let endFrameToSearchForPoint = maxFrameNumber - 1;
                {
                    let thisBallNextCollisionPointsData = lastStaticPointsFromStart.filter(pd => pd.frame >= startFrameToSearchForPoint);
                    thisBallNextCollisionPointsData = thisBallNextCollisionPointsData.filter(pd => pointsDataClassifiedToProcess[pd.class].includes(pd));
                    thisBallNextCollisionPointsData = _.sortBy(thisBallNextCollisionPointsData, 'frame');
                    if (thisBallNextCollisionPointsData.length) {
                        endFrameToSearchForPoint = thisBallNextCollisionPointsData[0].frame - 1;
                    }
                }
                let startRadiusToSearchForTwoPoints = imageBallRadiusServer * 8 + distanceBetweenLastPoints * 1;
                let endRadiusToSearchForTwoPoints = startRadiusToSearchForTwoPoints + distanceBetweenLastPoints * 10;
                let startDirectionCorridorWidth = imageBallRadiusServer * 2;
                let startDirectionCorridorLength = startRadiusToSearchForTwoPoints;

                let radiusToSearchForTwoPoints = startRadiusToSearchForTwoPoints;
                // let directionCorridorWidth = startDirectionCorridorWidth
                let directionCorridorLength = startDirectionCorridorLength;
                // {frameNumber: foundPointsDataInsideRadius}
                let foundPointsDataInsideRadiusByFrame = {};

                // точку пересечения сохраняем, строим направление к staticPointData, делаем коридор. 
                // Идем по z, пока не будет 2 точки в кадре (в предположительном радиусе). Все точки что в принципе были до этого, сохраняем.
                // Если точка не входит в коридор, то расширяем и удлиняем коридор. 
                // Когда нашли 2 точки на кадре (и одна из них входит в коридор), то входящая в коридор точка - это целевой шар.
                // если в коридор входят сразу 2 точки, то целевая та, у которой угол с направляющей меньше.
                for (let frameNumberToSearchForPoints = startFrameToSearchForPoint; frameNumberToSearchForPoints <= endFrameToSearchForPoint; frameNumberToSearchForPoints++) {
                    if (radiusToSearchForTwoPoints > endRadiusToSearchForTwoPoints) {
                        break;
                    }
                    framePointsDataToProcess = groupedByFrameToProcess[frameNumberToSearchForPoints];
                    if (!framePointsDataToProcess?.length) continue;

                    let isNextPointsFound = false;
                    let foundPointsDataInsideRadius = framePointsDataToProcess.filter(pd =>
                        utils.calculatePointsDistance(directionPointData.point, pd.point) <= radiusToSearchForTwoPoints);
                    foundPointsDataInsideRadiusByFrame[frameNumberToSearchForPoints] = foundPointsDataInsideRadius;
                    if (foundPointsDataInsideRadius.length >= 2) {
                        for (let countOfNotFoundCorridorAttempts = 0; countOfNotFoundCorridorAttempts < 3; countOfNotFoundCorridorAttempts++) {
                            let directionCorridorWidth = startDirectionCorridorWidth + countOfNotFoundCorridorAttempts * imageBallRadiusServer;
                            let deflection_startPoint_distance = utils.calculatePointsDistance(deflectionPoint, staticPointData.point);
                            // хорошо бы начало коридора сделать чуть дальше, но это бессмысленно - я не знаю скорость, с какой полетел целевой шар.
                            let corridorStartPoint1 = utils.getOrtogonalTrianglePointA(staticPointData.point, deflectionPoint, deflection_startPoint_distance, directionCorridorWidth, false)
                            let corridorStartPoint2 = utils.getOrtogonalTrianglePointA(staticPointData.point, deflectionPoint, deflection_startPoint_distance, directionCorridorWidth, true)

                            let corridorDirectionEndKoefficient = (deflection_startPoint_distance + directionCorridorLength) / deflection_startPoint_distance
                            let corridorDirectionEndPoint = utils.generateSegmentPoint(deflectionPoint, staticPointData.point, corridorDirectionEndKoefficient);
                            // почему тут true и false, а выше было false и true: потому что здесь направление обратное.
                            let corridorEndPoint1 = utils.getOrtogonalTrianglePointA(staticPointData.point, corridorDirectionEndPoint, directionCorridorLength, directionCorridorWidth, true)
                            let corridorEndPoint2 = utils.getOrtogonalTrianglePointA(staticPointData.point, corridorDirectionEndPoint, directionCorridorLength, directionCorridorWidth, false)

                            let corridor = [corridorEndPoint1, corridorEndPoint2, corridorStartPoint2, corridorStartPoint1];
                            let pointsDataInsideCorridorWithAngle = [];
                            for (let pointData of foundPointsDataInsideRadius) {
                                if (utils.checkIsPointInsideTetragon(pointData.point, corridor)) {
                                    pointsDataInsideCorridorWithAngle.push({ pointData })
                                }
                            }
                            // если точки в коридоре не найдены, значит попробовать увеличить коридор (то есть просто continue)
                            if (pointsDataInsideCorridorWithAngle < 1) continue;

                            for (let pointDataWithAngle of pointsDataInsideCorridorWithAngle) {
                                let pointData = pointDataWithAngle.pointData;
                                let angleDeg = utils.calculatePointsAngleDeg(staticPointData.point, pointData.point, staticPointData.point, corridorDirectionEndPoint);
                                pointDataWithAngle.angle = angleDeg;
                            }

                            // Если таких >1, значит найти нужную через сравнение угла между точкой и направляющей.
                            // если ровно 1, значит здесь точка целевого шара.
                            pointsDataInsideCorridorWithAngle = _.sortBy(pointsDataInsideCorridorWithAngle, 'angle');
                            // в targetBallNextPointData каким-то образом попадает staticPoint, т.е. возможна путаница с кадрами
                            let targetBallNextPointData = pointsDataInsideCorridorWithAngle[0].pointData;
                            // уточнить позицию точки соприкосновения, по найденной targetBallNextPointData.
                            let distanceFromStaticPointToNextPoint = utils.calculatePointsDistance(targetBallNextPointData.point, staticPointData.point);
                            deflectionPointData.point = utils.generateSegmentPoint(
                                targetBallNextPointData.point,
                                staticPointData.point,
                                (distanceFromStaticPointToNextPoint + imageBallRadiusServer * 2) / distanceFromStaticPointToNextPoint)
                            let thisBallNextPointData = null;
                            // if (pointsDataInsideCorridorWithAngle.length > 1) {
                            //     thisBallNextPointData = pointsDataInsideCorridorWithAngle[1];
                            // } else
                            // взять из точек в кадре ту точку, из тех что не является targetBallNextPointData, 
                            // и при этом находится ближе всех (по расстоянию) к cueBallDeflectionPoint. Углы тут не подойдут.
                            {
                                let foundPointsDataInsideRadiusWithoutTargetBallNextPoint = foundPointsDataInsideRadius.filter(pd => pd != targetBallNextPointData);
                                let foundPointsDataInsideRadiusWithoutTargetBallNextPointWithDistance = [] // расстояние до cueBallDeflectionPoint, не до staticPoint
                                for (let pointData of foundPointsDataInsideRadiusWithoutTargetBallNextPoint) {
                                    let distance = utils.calculatePointsDistance(pointData.point, deflectionPointData.point);
                                    foundPointsDataInsideRadiusWithoutTargetBallNextPointWithDistance.push({ distance, pointData });
                                }
                                foundPointsDataInsideRadiusWithoutTargetBallNextPointWithDistance = _.sortBy(foundPointsDataInsideRadiusWithoutTargetBallNextPointWithDistance, 'distance');
                                thisBallNextPointData = foundPointsDataInsideRadiusWithoutTargetBallNextPointWithDistance[0].pointData;
                            }
                            thisBallNextPointData.class = thisClassNumber;
                            pointsDataClassified[thisBallNextPointData.class].push(thisBallNextPointData)
                            targetBallNextPointData.class = staticPointData.class;
                            pointsDataClassified[targetBallNextPointData.class].push(targetBallNextPointData)
                            isNextPointsFound = true;



                            // todo теперь у меня есть 2 точки, которые принадлежат к разным шарам. Но могут быть пред. кадры с 1м шаром, которые непонятно чьи.
                            // проходим по ним, и смотрим к какой направляющей (по углу) они ближе, проставляем соотв. классы.

                            // foundPointsDataInsideRadiusByFrame, берем только фреймы до текущего не включая (чтобы не обрабатывать уже зафиксированные точки). 
                            // Текущий - это где нашли 2 следующие точки (целевого и ударного).
                            for (let [frameNumber, framePointsInRadiusData] of Object.entries(foundPointsDataInsideRadiusByFrame)) {
                                if (frameNumber == frameNumberToSearchForPoints) { continue; }
                                // если framePointsInRadiusData.length == 0, пропускаем
                                if (!framePointsInRadiusData.length) { continue };
                                // для длины >=2 это ошибка. Такие должны были уже обработаться
                                if (framePointsInRadiusData.length >= 2) {
                                    throw new Error('framePointsInRadiusData.length>=2, так не может быть, этот случай должен был раньше обработаться')
                                }
                                // если framePointsInRadiusData.length == 1, то эта точка или к текущему шару, или к целевому.
                                if (framePointsInRadiusData.length == 1) {
                                    let pointDataToProcess = framePointsInRadiusData[0];
                                    // определяем угол pointDataToProcess к thisBallNextPointData, и угол pointDataToProcess к targetBallNextPointData.
                                    // сравниваем, и понимаем к чему он ближе.
                                    let angleThisBall = utils.calculatePointsAngleDeg(deflectionPointData.point, thisBallNextPointData.point, deflectionPointData.point, pointDataToProcess.point);
                                    let angleTargetBall = utils.calculatePointsAngleDeg(staticPointData.point, targetBallNextPointData.point, staticPointData.point, pointDataToProcess.point);
                                    if (angleThisBall <= angleTargetBall)
                                    // это точка битка
                                    {
                                        pointDataToProcess.class = thisClassNumber;
                                    } else
                                    // это точка целевого шара
                                    {
                                        pointDataToProcess.class = staticPointData.class;
                                    }
                                    pointsDataClassified[pointDataToProcess.class].push(pointDataToProcess);
                                }
                            }


                            // проставляем новые startPointData и directionPointData, идущие по новому направлению текущего шара.
                            if (isNextPointsFound) {
                                if (utils.calculatePointsDistance(thisBallNextPointData.point, deflectionPointData.point) < imageBallRadiusServer) {
                                    continue;
                                }

                                startPointData = deflectionPointData;
                                directionPointData = thisBallNextPointData;
                                // break нужно вызывать только если таки нашли что надо...
                                break;
                            }
                        }


                    }

                    // directionCorridorWidth = startDirectionCorridorWidth;
                    radiusToSearchForTwoPoints += distanceBetweenLastPoints;
                    directionCorridorLength = radiusToSearchForTwoPoints;
                    if (isNextPointsFound) {
                        // кадр нужно увеличить, т.к. 1 или несколько кадров уже прошли...
                        currentFrameNumber += (frameNumberToSearchForPoints - startFrameToSearchForPoint + 1)
                        break;
                    }
                }
            }
        }

    }


    processedClassNumbers.push(thisClassNumber)
    return {
        processedClassNumbers
    }
}

function detectBall0DirectionPoints(sortedLastStaticPoints, pointsData) {
    // что тут нужно сделать: взять точки фреймов от первой до второй точки, которая будет двигаться (не включая).

    let cuePointData = sortedLastStaticPoints[0]
    let targetPointData = sortedLastStaticPoints[1]
    let firstFrameNumberToCheck = cuePointData.frame;
    let lastFrameNumberToCheck = targetPointData.frame - 2;
    let pointsDataToCheck = pointsData.filter(pd => pd.frame >= firstFrameNumberToCheck && pd.frame <= lastFrameNumberToCheck);

    // построить 5 коридоров: 
    // 1. прямой до шара
    // 2. отскок от борта слева
    // 3. отскок от борта справа
    // 4. отскок от борта сверху
    // 5. отскок от борта снизу
    // точка отскока как-то должна высчитываться из разницы координат.
    let trajectories = {
        'straight': {
            corridorsToCheck: [{ start: {}, end: {} }],
            foundPointsData: [],
            directionStartPoint: {},
            directionEndPoint: {}
        },
        'sideY0': {},
        'sideYmax': {},
        'sideX0': {},
        'sideXmax': {},
    }
    // заполняем точки коридоров
    for (let [trajectoryName, trajectoryValue] of Object.entries(trajectories)) {
        switch (trajectoryName) {
            case 'straight': {
                trajectoryValue.corridorsToCheck = [{ start: cuePointData, end: targetPointData }]
                trajectoryValue.directionStartPoint = cuePointData;
                trajectoryValue.directionEndPoint = targetPointData;
                break;
            }

            // для бортов: 1й это от начала до точки на борте, 2й это от точки на борте до конца: 
            case 'sideY0': {
                // нужно определить точку на борте
                let dx = targetPointData.
                    point.x - cuePointData.point.x;
                let ky = (cuePointData.point.y / targetPointData.point.y)
                let perfectkx = 0.55;
                let kx;
                if (ky >= 1) {
                    kx = 1 - (1 - perfectkx) / ky
                } else {
                    kx = 0 + perfectkx * ky
                }

                let sidePointX = cuePointData.point.x + dx * kx;
                let sidePointY = 0;
                let sidePointData = { point: { x: sidePointX, y: sidePointY } }

                let firstCorridor = { start: cuePointData, end: sidePointData }
                let secondCorridor = { start: sidePointData, end: targetPointData }

                trajectoryValue.directionStartPoint = secondCorridor.start;
                trajectoryValue.directionEndPoint = secondCorridor.end;
                trajectoryValue.corridorsToCheck = [firstCorridor, secondCorridor]
                break;
            }

            case 'sideYmax': {
                // нужно определить точку на борте
                let dx = targetPointData.point.x - cuePointData.point.x;
                let ky = (serverCanvasHeight - cuePointData.point.y) / (serverCanvasHeight - targetPointData.point.y)
                let perfectkx = 0.55;
                let kx;
                if (ky >= 1) {
                    kx = 1 - (1 - perfectkx) / ky
                } else {
                    kx = 0 + perfectkx * ky
                }

                let sidePointX = cuePointData.point.x + dx * kx;
                let sidePointY = serverCanvasHeight;
                let sidePointData = { point: { x: sidePointX, y: sidePointY } }

                let firstCorridor = { start: cuePointData, end: sidePointData }
                let secondCorridor = { start: sidePointData, end: targetPointData }

                trajectoryValue.directionStartPoint = secondCorridor.start;
                trajectoryValue.directionEndPoint = secondCorridor.end;
                trajectoryValue.corridorsToCheck = [firstCorridor, secondCorridor]
                break;
            }

            case 'sideX0': {
                // нужно определить точку на борте
                let dy = targetPointData.point.y - cuePointData.point.y;
                let kx = (cuePointData.point.x / targetPointData.point.x)
                let perfectky = 0.55;
                let ky;
                if (kx >= 1) {
                    ky = 1 - (1 - perfectky) / kx
                } else {
                    ky = 0 + perfectky * kx
                }

                let sidePointX = 0;
                let sidePointY = cuePointData.point.y + dy * ky;
                let sidePointData = { point: { x: sidePointX, y: sidePointY } }

                let firstCorridor = { start: cuePointData, end: sidePointData }
                let secondCorridor = { start: sidePointData, end: targetPointData }

                trajectoryValue.directionStartPoint = secondCorridor.start;
                trajectoryValue.directionEndPoint = secondCorridor.end;
                trajectoryValue.corridorsToCheck = [firstCorridor, secondCorridor]
                break;
            }

            case 'sideXmax': {
                // нужно определить точку на борте
                let dy = targetPointData.point.y - cuePointData.point.y;
                let kx = (serverCanvasWidth - cuePointData.point.x) / (serverCanvasWidth - targetPointData.point.x)
                let perfectky = 0.55;
                let ky;
                if (kx >= 1) {
                    ky = 1 - (1 - perfectky) / kx
                } else {
                    ky = 0 + perfectky * kx
                }

                let sidePointX = serverCanvasWidth;
                let sidePointY = cuePointData.point.y + dy * ky;
                let sidePointData = { point: { x: sidePointX, y: sidePointY } }

                let firstCorridor = { start: cuePointData, end: sidePointData }
                let secondCorridor = { start: sidePointData, end: targetPointData }

                trajectoryValue.directionStartPoint = secondCorridor.start;
                trajectoryValue.directionEndPoint = secondCorridor.end;
                trajectoryValue.corridorsToCheck = [firstCorridor, secondCorridor]
                break;
            }

            default: break;
        }
    }


    // заполняем точки, попавшие в коридоры, делаем _.unique чтобы избежать дублей, попавших в оба коридора
    for (let [trajectoryName, trajectoryValue] of Object.entries(trajectories)) {
        let foundPointsData = []
        for (let corridorStartEnd of trajectoryValue.corridorsToCheck) {
            let corridorFoundPointsData = []
            let { start: corridorStartPointData, end: corridorEndPointData } = corridorStartEnd;
            let corridorStartPoint = corridorStartPointData.point;
            let corridorEndPoint = corridorEndPointData.point;

            let corridorLength = utils.calculatePointsDistance(corridorStartPoint, corridorEndPoint);
            let corridorWidth = imageBallRadiusServer * 4;
            let corridorStartPoint1 = utils.getOrtogonalTrianglePointA(corridorEndPoint, corridorStartPoint, corridorLength, corridorWidth, true)
            let corridorStartPoint2 = utils.getOrtogonalTrianglePointA(corridorEndPoint, corridorStartPoint, corridorLength, corridorWidth, false)

            let corridorEndPoint1 = utils.getOrtogonalTrianglePointA(corridorStartPoint, corridorEndPoint, corridorLength, corridorWidth, false)
            let corridorEndPoint2 = utils.getOrtogonalTrianglePointA(corridorStartPoint, corridorEndPoint, corridorLength, corridorWidth, true)

            let corridor = [corridorEndPoint1, corridorEndPoint2, corridorStartPoint2, corridorStartPoint1]
            for (let pd of pointsDataToCheck) {
                if (utils.checkIsPointInsideTetragon(pd.point, corridor)) {
                    corridorFoundPointsData.push(pd)
                }
            }

            foundPointsData.push(...corridorFoundPointsData);
        }
        foundPointsData = _.uniq(foundPointsData)

        trajectoryValue.foundPointsData = foundPointsData;
    }

    // находим из trajectories запись с максимумом найденных точек, возвращаем её.
    let trajectoryWithMaxPoints = _.maxBy(Object.values(trajectories), value => value.foundPointsData?.length)
    trajectoryWithMaxPoints.foundPointsData = _.sortBy(trajectoryWithMaxPoints.foundPointsData, 'frame')
    trajectoryWithMaxPoints.directionEndPoint = _.last(trajectoryWithMaxPoints.foundPointsData)
    return trajectoryWithMaxPoints
}

function detectBall0DirectionPointData(minFrameNumber, maxFrameNumber, groupedByFrame, startPointData) {
    let directionPointData;
    for (let currentFrameNumber = minFrameNumber + 1; currentFrameNumber <= maxFrameNumber; currentFrameNumber++) {
        let framePointsData = groupedByFrame[currentFrameNumber];

        if (!framePointsData?.length) {
            continue;
        }
        // if (framePointsData.length > 1) {
        //     console.warn('Наличие >1 шара при попытке определить направление битка'); // очередной глюк исходных данных. 
        // }
        if (framePointsData.length) {
            let framePointsDataWithDistance = framePointsData.map(pd => ({ pointData: pd, distance: utils.calculatePointsDistance(pd.point, startPointData.point) }))
            framePointsDataWithDistance = _.sortBy(framePointsDataWithDistance, 'distance');
            if (framePointsDataWithDistance[0].distance < imageBallRadiusServer / 2) { continue; }
            directionPointData = framePointsDataWithDistance[0].pointData;
            break;
        }
    }
    return directionPointData;
}

// sectorsToSearch: или circle (самое простое), или tetragon, или tetragonWithCircle. 
// Пример: {type: 'circle', r: 10}
// Пример: {type: 'tetragon', points[4]: [p1,p2,p3,p4]}
// если находится несколько точек, то берется ближайшая к линии startPoint-directionPoint. Если нет directionPoint, то ближайшая к startPoint.
function detectBallNextPointData(minFrameNumber, maxFrameNumber, groupedByFrame, startPoint, directionPoint, sectorsToSatisfy) {


}

// maxAnomalyDistance: если точки нет внутри этого расстояния в течении 2х кадров, значит она начала движение.
// Т.к. у Димы везде есть погрешность, то точка может определиться за этим радиусом, но потом быстро "вернуться" обратно.
// Логика:
// Сначала нужно найти скопление точек. Для этого берем первый кадр (нужно найти, какой из них первый), 
// и ищем точки в последующих frame, которые находятся примерно в том же месте. 
// Достаточно 100 кадров. Затем считаем среднюю точку из них всех.
// Далее можно определить, какие шары статичные (т.е. область, в которой всегда есть шар (с учетом maxAnomalyFrames)) до самого конца удара.
// Статические области мы вообще убираем из фильтров и расчетов, так будет быстрее.
// После этого нужно определить начало движения, и убрать из фильтров точки шара "до начала".
// По принципу "шар начал двигаться, когда ни одного нет там, где шар был раньше" - это подходит только для начала удара, т.е. удара по битку.
// 
// То есть подзадача: определить начало движения битка, т.е. какой шар начал двигаться раньше по принципу "теперь шара нет там, где он есть".
// Для упрощения себе задачи на первом этапе, я лучше возьму первые 1000 кадров (чтобы не обрабатывать конец удара, где биток почти стоит), и внутри них буду всё считать и искать.
// на выходе: массив точек без статического мусора, инфа "последний в теории статический кадр". 
// Важно что при обнаружении второго шара вблизи (maxOtherBallDistance) (где его раньше не было) нужно прекратить сравнения, т.к. второй шар может стать на место первого.
// нужно было бы сразу посчитать коридоры между всеми шарами (не статическими), но т.к. у нас есть борта, это имеет мало смысла.

// 5 кадров это 83мс при 60fps, 166.6 мс при 30fps, и 217 мс при 23(24)fps. Поэтому maxAnomalyFrames пусть будет 3 кадра.
export function detectPointsStartsAndDeleteStaticFromBeginAndEnd(shot, maxOtherBallDistance = 50, maxAnomalyDistance = imageBallRadiusServer) {
    shot.pointsCloud = _.sortBy(shot.pointsCloud, 'frame');
    let allPointsData = shot.pointsCloud;
    let minFrameNumber = allPointsData.first().frame;
    let maxFrameNumber = allPointsData.last().frame;

    let groupedByFrame = _.groupBy(allPointsData, 'frame');
    let firstFrame = groupedByFrame[minFrameNumber];
    let lastFrame = groupedByFrame[maxFrameNumber];

    // первый фрейм: Дима сказал что на первом фрейме точно все есть, так что можно от него играть.
    // console.log('firstFrame is', firstFrame)
    // сейчас frame (firstFrame) это массив точек.
    let [staticDataToDelete, midStaticPoints] = detectStaticBeginning(false, firstFrame, lastFrame, minFrameNumber, maxFrameNumber, groupedByFrame, maxAnomalyDistance)
    let [staticDataToDelete2, lastStaticPointsFromEnd] = detectStaticBeginning(true, firstFrame, lastFrame, minFrameNumber, maxFrameNumber, groupedByFrame, maxAnomalyDistance)
    let pointsDataWithoutStatic = _.pullAll(_.clone(allPointsData), [...staticDataToDelete, ...staticDataToDelete2]);
    // если удар начинается с 1го кадра (кроме staticDataToDelete), то биток может не определиться статической группой. Тогда
    // на lastStaticPointsFromEnd это не относится
    let minFrameWithoutStatic = _.minBy(pointsDataWithoutStatic, 'frame');
    let minFrameOfLastStaticPoints = _.minBy(midStaticPoints, 'frame');
    if (!minFrameOfLastStaticPoints || !minFrameWithoutStatic) return {}
    if (minFrameWithoutStatic.frame < minFrameOfLastStaticPoints.frame && utils.calculatePointsDistance(minFrameWithoutStatic.point, minFrameOfLastStaticPoints.point) >= maxAnomalyDistance) {
        midStaticPoints.push(minFrameWithoutStatic);
    }

    groupedByFrame = _.groupBy(pointsDataWithoutStatic, 'frame');

    let lastStaticPointsFullyStatic = midStaticPoints.filter(pd => pd.frame == maxFrameNumber);
    let lastStaticPointsFromEndFullyStatic = lastStaticPointsFromEnd.filter(pd => pd.frame == minFrameNumber);
    midStaticPoints = midStaticPoints.filter(pd => pd.frame != maxFrameNumber)
    lastStaticPointsFromEnd = lastStaticPointsFromEnd.filter(pd => pd.frame != minFrameNumber)

    return { dataWithoutStatic: pointsDataWithoutStatic, lastStaticPoints: midStaticPoints, lastStaticPointsFromEnd, lastStaticPointsFullyStatic, lastStaticPointsFromEndFullyStatic };
}

// todo если isReverseDirection, то ищем статику не с начала, а с конца.
export function detectStaticBeginning(isReverseDirection, firstFrame, lastFrame, minFrameNumber, maxFrameNumber, groupedFramesData, maxAnomalyDistance, maxAnomalyFrames = 5) {
    // zNumber: [pointData, pointData, pointData]
    let totalStaticData = [...firstFrame];
    // 
    let lastStaticPoints = []

    // groupedFramesData[maxZ] существует, это длина массива
    // просто идем и смотрим наличие точки там же где она была на firstFrame.
    if (!isReverseDirection) {
        for (let pointData of firstFrame) {
            let lastStaticPointData = { minFrameNumber: minFrameNumber, pointData }; // последний статический z для этой точки из firstFrame
            let currentFrameNumber;

            let pointStaticDatas = []
            for (currentFrameNumber = minFrameNumber + 1; currentFrameNumber <= maxFrameNumber; currentFrameNumber++) {
                let currentFrame = groupedFramesData[currentFrameNumber];
                if (!currentFrame.length) continue; // если во фрейме не обнаружено ни одной точки, то этот кадр умно обработать невозможно. Лучше просто пропустить.
                let [lowestDistanceCurrent, pointDataWithLowestDistance] = calculateLowestDistanceForPointsData(pointData, currentFrame)
                if (lowestDistanceCurrent <= maxAnomalyDistance) {
                    lastStaticPointData.minFrameNumber = currentFrameNumber;
                    lastStaticPointData.pointData = pointDataWithLowestDistance;
                    totalStaticData.push(pointDataWithLowestDistance)
                    pointStaticDatas.push(pointDataWithLowestDistance)
                } else if (currentFrameNumber - lastStaticPointData.minFrameNumber > maxAnomalyFrames) {
                    // в lastStaticZ - последняя статичная точка. Куда именно пошло движение, я не могу знать. Там только искать по увеличенному радиусу.
                    // lastStaticPoints.push(pointDataWithLowestDistance);
                    break;
                }
            }
            // значит что массив пройден без добавления в lastStaticPoints - точка статична до самого конца
            if (currentFrameNumber > maxFrameNumber) {
                // lastStaticPoints.push(lastStaticPointData.pointData);
            }
            let midX = _.meanBy(pointStaticDatas, pd => pd.point.x);
            let midY = _.meanBy(pointStaticDatas, pd => pd.point.y);
            lastStaticPointData.pointData.point.x = midX;
            lastStaticPointData.pointData.point.y = midY;
            lastStaticPoints.push(lastStaticPointData.pointData);
        }
    } else
    // isReverseDirection
    {
        for (let pointData of lastFrame) {
            let lastStaticPointData = { minFrameNumber: maxFrameNumber, pointData }; // последний статический z для этой точки из firstFrame
            let currentFrameNumber;

            for (currentFrameNumber = maxFrameNumber - 1; currentFrameNumber >= minFrameNumber; currentFrameNumber--) {
                let currentFrame = groupedFramesData[currentFrameNumber];
                if (!currentFrame.length) continue; // если во фрейме не обнаружено ни одной точки, то этот кадр умно обработать невозможно. Лучше просто пропустить.
                let [lowestDistanceCurrent, pointDataWithLowestDistance] = calculateLowestDistanceForPointsData(pointData, currentFrame)
                if (lowestDistanceCurrent <= maxAnomalyDistance) {
                    lastStaticPointData.minFrameNumber = currentFrameNumber;
                    lastStaticPointData.pointData = pointDataWithLowestDistance;
                    totalStaticData.push(pointDataWithLowestDistance)
                } else if (lastStaticPointData.minFrameNumber - currentFrameNumber > maxAnomalyFrames) {
                    // в lastStaticZ - последняя статичная точка. Куда именно пошло движение, я не могу знать. Там только искать по увеличенному радиусу.
                    lastStaticPoints.push(pointDataWithLowestDistance);
                    break;
                }
            }
            // значит что массив пройден без добавления в lastStaticPoints - точка статична до самого конца
            if (currentFrameNumber < minFrameNumber) {
                lastStaticPoints.push(lastStaticPointData.pointData);
            }
        }
    }


    // lastStaticPoints это тоже массив pointData. Ячейка в этом массиве: всё что до него, и это место включительно - статическая точка.
    // статическую точку можно не учитывать в расчетах (когда ищем динамические точки в области), 
    // а можно иметь в виду, когда видим что шар направляется в неё. Бывает ситуация, когда биток остается прямо на месте целевого шара. 
    // И тогда это получится статическая точка, но уже другим шаром.
    return [totalStaticData, lastStaticPoints];
}

function calculateLowestDistanceForPointsData(pointData, pointsToCheck) {
    // [distance, pointData]
    let distancesWithPoint = []
    for (let pointToCheck of pointsToCheck) {
        let distance = utils.calculatePointsDistance(pointData.point, pointToCheck.point);
        distancesWithPoint.push([distance, pointToCheck]);
    }

    let minDistanceWithPointData = _.minBy(distancesWithPoint, pair => pair[0]);
    return minDistanceWithPointData;
}
