import _ from 'lodash';

Number.prototype.roundDecimals = function (decimals) {
  let entry = this;
  for (let i = 0; i < decimals; i++) {
    entry *= 10;
  }
  let rounded = Math.round(entry);
  let result = rounded;
  for (let i = 0; i < decimals; i++) {
    result /= 10;
  }
  return result;
}

Number.prototype.floor = function () {
  return Math.floor(this);
}

Number.prototype.ceil = function () {
  return Math.ceil(this);
}

Number.prototype.round = function () {
  return Math.round(this);
}

Number.prototype.sqr = function () {
  return this * this;
}

Number.prototype.sqrt = function () {
  return Math.sqrt(this);
}

Array.prototype.isEmpty = function () {
  return this.length == 0;
}

Array.prototype.isNotEmpty = function () {
  return this.length != 0;
}

Array.prototype.first = function () {
  return _.first(this);
}

Array.prototype.last = function () {
  return _.last(this);
}

Array.prototype.insert = function (idx, item) {
  this.splice(idx, 0, item);
}

Array.prototype.insertAll = function (idx, items) {
  this.splice(idx, 0, ...items);
}

Array.prototype.add = function (item) {
  return this.push(item);
}

Array.prototype.addAll = function (items) {
  return this.push(...items);
}

// Object.prototype.clone = function () {
//   return _.clone(this);
// }