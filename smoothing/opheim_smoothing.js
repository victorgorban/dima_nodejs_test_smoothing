import * as utils from './utils.js';
import { serverCanvasWidth, serverCanvasHeight } from './constants.js';
import './extensions.js';

const pointDimensions = 2;
const DIM = pointDimensions;

/// 2-мерный вектор из 2х точек. Как обычно, но представление точки идет массивом из 2х значений.
/// TODO переписать бы всю эту фигню на нормальные точки...
function makeVector(p1, p2) {
  return { dx: p2.dx - p1.dx, dy: p2.dy - p1.dy };
}

/// умножение координат 2-мерного вектора. Как обычно, но представление точки/вектора идет массивом из 2х значений.
function vectorDot(v1, v2) {
  const result = v1.dx * v2.dx + v1.dy * v2.dy;

  return result;
}

/// интерполяция между двумя точками. По-моему, это ничем не отличается от моего generateSegmentPoint.
// точки - {dx, dy}
function interpolate(
  p1,
  p2,
  fraction,
) {
  return utils.generateSegmentOffset(p1, p2, fraction);
}

/// разница квадратов координат.
// точки - {dx, dy}
function point_distance2(p1, p2) {
  let result = 0;
  result = (p1.dx - p2.dx).sqr() + (p1.dy - p2.dy).sqr();
  return result;
}

/// расстояние до луча (луч представлен отрезком) и точкой в квадрате.
// точки - {dx, dy}
function ray_distance2(rayStart, rayPoint, point) {
  // вектор луча
  let rayVector = makeVector(rayStart, rayPoint);
  // вектор от начала луча до точки
  let rayPointVector = makeVector(rayStart, point);

  let scalarNumberRayRay =
    vectorDot(rayVector, rayVector); // squared length of v
  let scalarNumberRayPoint =
    vectorDot(rayPointVector, rayVector); // project w onto v

  if (scalarNumberRayPoint <= 0) {
    // projection of w lies to the left of r1 (not on the ray)
    return point_distance2(point, rayStart);
  }

  // avoid problems with divisions when value_type is an integer type
  let fraction =
    scalarNumberRayRay == 0 ? 0 : scalarNumberRayPoint / scalarNumberRayRay;

  // здесь линейная интерполяция. Обычно квадратичная дает более точные результаты.
  // p projected onto ray (r1, r2)
  let proj = interpolate(rayStart, rayPoint, fraction);

  return point_distance2(point, proj);
}

/// [offsetPath]: полилиния для сглаживания
/// [minTolerance]: все точки внутри круга с этим радиусом будут считаться как избыточные. Также это макс. отклонение
/// [maxTolerance]: длина (в координатах) луча, внутри которого производится анализ лишних точек (сглаживание).
/// [maxTolerance] по моим наблюдениям стоит брать или в 5 раз больше, или в 10 раз больше [minTolerance]
export function smoothenLineOpheim(
  offsetPath, minTolerance, maxTolerance) {
  // В оригинальном алгоритме offsetPath это был вектор из чисел, а не координат. Поэтому придется превращать это в вектор координат.

  // что здесь происходит: оригинальный алгоритм работает не с координатами, а с вектором координат, т.е. точка {24, 12} это 2 значения: 24, 12.
  // чтобы привести исходные данные к требованиям этого бредового алгоритма, я превращу initialPath в этот массив сначала.
  let offsetResult = [];

  // int coordsCount = offsetPath.length * 2;
  let pointsCount = offsetPath.length;
  let minTol2 = minTolerance.sqr(); // squared minimum distance tolerance
  let maxTol2 = maxTolerance.sqr(); // squared maximum distance tolerance

  // validate input and check if simplification required
  if (pointsCount < 3 || minTol2 == 0 || maxTol2 == 0) {
    return [...offsetPath];
  }

  // define the ray R(r0, r1)
  let rayStart =
    offsetPath.first(); // indicates the current key and start of the ray
  let rayPoint = offsetPath.first(); // indicates a point on the ray
  let isRayDefined = false;

  // keep track of two test points
  let prevPoint = offsetPath[0]; // the previous test point
  let currentPoint = offsetPath[1]; // the current test point (pi+1)
  // AdvanceCopy (pi);

  // the first point is always part of the simplification
  offsetResult.add(rayStart);
  // CopyKey (r0, result);

  for (let i = 2; i < pointsCount; ++i) {
    prevPoint = offsetPath[i - 1];
    currentPoint = offsetPath[i];

    let pointDistance2FromRayStart = point_distance2(rayStart, currentPoint);

    if (!isRayDefined) {
      // discard each point within minimum tolerance
      if (pointDistance2FromRayStart < minTol2) {
        continue;
      }
      // the last point within minimum tolerance pi defines the ray R(r0, r1)
      rayPoint = prevPoint;
      isRayDefined = true;
    }

    // check each point pj against R(r0, r1)
    if (pointDistance2FromRayStart < maxTol2 &&
      ray_distance2(rayStart, rayPoint, currentPoint) < minTol2) {
      continue;
    }
    // found the next key at pi
    offsetResult.add(prevPoint);
    // CopyKey (pi, result);
    // define new ray R(pi, pj)
    rayStart = prevPoint;
    isRayDefined = false;
  }
  // the last point is always part of the simplification
  offsetResult.add(currentPoint);
  // CopyKey (pj, result);

  return offsetResult;
}
