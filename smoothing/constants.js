export const serverCanvasWidth = 1280;
export const serverCanvasHeight = 640;

// 0.028 / 1.53 - соотношение радиуса шара к малой стороне стола. 57мм - диаметр шара, 1.53м - ширина узкой стороны 9-футового стола.
export const imageBallRadiusServer = 0.028 / 1.53
    *
    serverCanvasHeight;