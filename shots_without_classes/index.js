import shot1 from './77NHPyMYgtSREEwwp.js'
import shot2 from './AQBhXfr7BJMkEGbBy.js'
import shot3 from './CkvKEamRgZMbq6yhW.js'
import shot4 from './CrjJm8LH2K5THBHW8.js'
import shot5 from './MHJpFPvtkMW9FnZ3A.js'
import shot6 from './NnBLPPp3tPWFqwPt6.js'
import shot7 from './nPh9e74Efh8NWAcoq.js'
import shot8 from './shmNCtGgroc9eQa9e.js'
import shot9 from './vhASz3PmZS6ftxXqC.js'

export default [
    shot1,
    shot2,
    shot3,
    shot4,
    shot5,
    shot6,
    shot7,
    shot8,
    shot9,
];